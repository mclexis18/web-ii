const Express = require('express');
const App = Express();
const Rutas = require('./Router/Birds');

/**Podemos Acceder a todas las rutas que definimos en el archivo Birds
 * la Url sería algo como 
 * localhost:8080/Rutas/About 
 * localhost:8080/Rutas/Index
 */
App.use('/Rutas', Rutas); 

App.listen(8080, ()=> console.log('Corriendo'));