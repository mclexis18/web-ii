const Express = require('express');
const Routers = Express.Router();

/**Inicializamos la conexion de las rutas */
Routers.use('/', (req, res, next)=>
{
    console.log('Usted accedio a las', Date.now());
    next();
});
/**we create our two routers */
Routers.get('/Index', (req, res)=>
{
    res.send('Welcome to Index');
});
Routers.get('/About', (req, res)=>
{
    res.send('what do you mean about our services');

});

/**exports our routers */
module.exports = Routers;
