const Express = require('express');
const App = Express();
const {PORT} = require('./config');

/**la ruta original sera la de localhost con el puerto que pasemos en el listen, dentro de esa ruta vamos a enviar
 * lo siguiente
 */
App.get('/Linux', (req, res)=>
{
    /**En el navegador nos saldrá algo como localhost:8080/Linux y como cuerpo el Hola Mundo */
    res.send('Hola Mundo');
    res.end();

});
/**Mandaremos un arreglo */
const DatosEstudiante = [ {Nombre:"Alexis Lopez", Edad:22} ]
App.get('/Arreglo', (req, res)=>
{
    res.send(DatosEstudiante[0].Nombre);
    res.end();
});

/**Metodo post */
App.post('/Linux', (req, res)=>
{
    /**En el navegador nos saldrá algo como localhost:8080/Linux y como cuerpo el Hola Mundo */
    res.send('Hola Mundo');
    res.end();

});

/**Netodo put */
App.put('/Linux', (req, res)=>
{
    res.send('Hola Mundo');
    res.end();
});

/**Archivos estaticos de esta manera podemos acceder a las imagenes que hay dentro de la carpeta
 * public la ruta will be something like 
 * localhost:8080/Imagen.png 
 */
/**Via de acceso Normal */
//App.use(Express.static('Public'));
/**Via de acceso abdoluta  Para mayor control if you 
 * want get date unitl another file
*/
App.use(Express.static(__dirname + '/Public'));


/**Manejo de Varias Rutas */

App.get('/Multiples', (req, res, next)=>
{
    console.log('Siga Avanzando será en la siguiente');
    /**next le da el paso a la siguiente ruta  */
    next();
},
    (req, res)=>
    {
        res.send('hi everyone I am here once again');
    }
);


/**Opciones Nuevas en las funciones */
const Valor1 =(req, res, next)=>
{
    if (5>10) 
    {
        res.send('Aqui esta el resultado valor 1');
    }else 
    {
        console.log('Sigue participando');
        next();
    }
}
const Valor2 =(req, res, next)=>
{
    if (5>10) 
    {
        res.send('Aqui esta en resultado Valor2');
    }else 
    {
        console.log('Sigue participando');
        next();
    }
}
const Valor3 =(req, res, next)=>
{
    if (12>10) 
    {
        res.send('Aqui esta en resultado Valor3');
    }else 
    {
        console.log('Sigue participando');
        next();
    }
}

   /**Este ejemplo dara como resultado que se salta 2 veces y el resultado estara en la tercera, puesto 
    * que ahi es donde esta la opcion correcta 
    */
  App.get('/example/c', [Valor1, Valor2, Valor3]);
  
  

App.listen(PORT, ()=> console.log("corriendo"));