const {Schema, model} = require('mongoose');

const Mensajes = new Schema(
    {
        Mensaje:{type:String}
    });

module.exports = model("Mensajes", Mensajes);