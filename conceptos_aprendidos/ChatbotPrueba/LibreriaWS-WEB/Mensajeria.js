const PruebaWhatsapp = require('whatsapp-web.js');
const Cliente = new PruebaWhatsapp.Client();
const CodigoQR = require('qrcode-terminal'); 
const {CADENA_CONEXION} = require('../Config');
const {connect}= require('mongoose');
const {Mensajes} = require('../Models');

 

Cliente.on('qr',(qr)=>
{
    CodigoQR.generate(qr, {small:true});
});
Cliente.on('ready', ()=>
{
    console.log('cliente succesful');
});

const DatosMensaje = 
{
    Nombre:'Alex'
};


Cliente.on('message', async msg=>
{

    const {author, body, from} = msg;
    console.log( body, from);
    if (msg.body === 'Bienvenido') 
    {
        msg.reply('Casilla sorpresa elige 1, 2 , 3');    
    }
    if (msg.body === '1') 
    {
            msg.reply('felicidades hoy es tu dia');
    }
    if(msg.body === '2')
    {
        msg.reply('uy parce usted esta muy de malas');
    }
    if (msg.body === '3')
    {
        msg.reply('sigue intentando hoy no es tu dia ');    
    }

    const msj = new Mensajes({Mensaje:msg.body});
    msj.save();
    
});


Cliente.initialize();

module.exports = Cliente;