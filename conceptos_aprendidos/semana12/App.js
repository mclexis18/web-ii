const Express =require('express');
const App = Express();
const Axios = require('axios').default;

let URL = 'http://localhost:8080/api/v1/cursos';


/**Incluimos las vistas desde esa direccion */
App.set('views', './src/views');

/**Decimos que trabajamos con el motor de renderizado de pug */
App.set('view engine', 'pug');

/**en Estas partes accedemos a las rutas de las vistas.pug, creando nuestras rutas desde express */
const DatosCursos = Axios.get('http://localhost:8080/api/v1/cursos');
App.get('/Publico', async(req, res)=>{

    const Cursos = await DatosCursos;
    res.render('index', {
        /**Aquello que esta aqui ya esta definido en el file .pug */
            Cabezera:'Probando Pug',
            title:'Silencio Bruno',
            Contenido: 'Entrando Bruno',
            Cursos:Cursos.data 
    });

});

App.post('/Publico', (req, res)=>{

    

});

/**here is another route because we want to pull apart everything */
App.get('/Invitados', (req, res)=>{

    res.render('invite', 
            {
                title:'Invitados',
                Contenido:'Bienvenidos todos'
            });
});

/**Accordin to the arrays let's go with the next example */
let Ojos = ['azules', 'verdes', 'negros']
let Cara = ['Flaca', 'gorda', 'desfigurada']
let Persona = 'Alexis Lopez'

const Datos = {Ojos:Ojos, Cara:Cara, Persona:Persona}

App.get('/Interpolacion', (req, res)=>{

    res.render('Interpolacion', Datos);

});

/**we show how we can make html from the path */
const VariableDatos = '<div>This message are here</div>'

App.get('/datos', (req, res)=>{

    res.render('datos', {MyData:VariableDatos});

});


/**we create user to try show in the page */
const Myuser = {
    Nombre:'Alex Lopez',
    Id:'99',
    Direccion:'Calle 210'
}

/**we will create a function in the file Usuarios.pug  */

App.get('/Usuarios', (req, res)=>{

    res.render('Usuarios', {MyUser:Myuser});

});

/**we'll go througth a json file */
/**we'll create a json file */

const DatosEstudiantes = [
    
    {
        Nombre:'Alexis',
        Apellido:'Lopez',
        Nota:'7'
    },
    {
        Nombre:'Nardine',
        Apellido:'Alsa',
        Nota:'9'
    },
    
    {
        Nombre:'Karen',
        Apellido:'Santana',
        Nota:'8'
    }
]

App.get('/Estudiantes', (req, res)=>{

    res.render('Estudiantes', {
        DatosEstudiantes:DatosEstudiantes
    });

});


/**let's go running */
App.listen(5000, ()=>{
    
    console.log('corriendo bruno');

})