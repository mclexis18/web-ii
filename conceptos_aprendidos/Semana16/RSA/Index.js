const Rsa = require('node-rsa');
const Fs = require('fs');

/**Metodo para generar las llaves tanto publicas como privadas */
const Llaves = new Rsa().generateKeyPair();

/**Creamos nuestra llave publica */
const LlavePublica = Llaves.exportKey('public');

/**creamos nuestra llave privada */

const LlavePrivada = Llaves.exportKey('private');

/**Abrimos el archivo Publico creado en la carpeta llaves */
Fs.openSync('./keys/Public.pem', 'w');

/**Escribimos en ese archivo nuestra clave publica en formato utf8 */
Fs.writeFileSync('./keys/Public.pem', LlavePublica, 'utf8');


/** Abrimos el archivo Privado en la carpeta llaves */
/**en caso de no existir el archivo, este lo crea  */
Fs.openSync('./keys/Private.pem', 'w');

/**Escribimos en nuestro archivo la clave privada */

Fs.writeFileSync('./keys/Private.pem', LlavePrivada, 'utf8');


