const Rsa = require('node-rsa');
const Archivo = require('fs');

/**we create a key public and a key private*/
let KeyPublic = new Rsa();
let KeyPrivate = new Rsa();

/**we call a file his name is Public.pem and Private.pem*/
let FilewithkeyPublic = Archivo.readFileSync('./keys/Public.pem', 'utf8');
let FilewithkeyPrivate = Archivo.readFileSync('./keys/Private.pem', 'utf8');

/**we import that keys and call file at filewithkeypublic andfilewithkeyprivate*/
KeyPrivate.importKey(FilewithkeyPrivate);
KeyPublic.importKey(FilewithkeyPublic);


/**so we can create two messages to send that message at encripty */
const Message1 = 'Hello world Im here once again';
const Message2 = 'Hola mundo en español';


const EncryptFisrt = KeyPublic.encrypt(Message2, 'base64');
const withoutEncrypt = KeyPrivate.decrypt(EncryptFisrt, 'utf8');

console.log(EncryptFisrt);
console.log('este es un salto');
console.log(withoutEncrypt);