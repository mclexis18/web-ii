const Express = require('express');
const Mensajes = require('../Controller/Mensajes');


module.exports = function ({MensajesController}) {

    const Rutas = Express();

    Rutas.get('/', MensajesController.Index);

    return Rutas
    
}