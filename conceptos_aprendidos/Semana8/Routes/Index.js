const Express = require('express');

module.exports = function ({MensajesRoutes}) {

    const RutasHijas = Express();
    const RutasPadre = Express();

    RutasHijas.use('/Home', MensajesRoutes);

    RutasPadre.use('/Aplicacion1', RutasHijas);

    return RutasPadre
    
} 