const {createContainer, asClass, asFunction, asValue} =require('awilix');
const Contenedor = createContainer();

const Rutas = require('../Routes');
const MensajesController = require('../Controller');
const  MensajesRoutes = require('../Routes/Mensajes.Routes');
const App = require('.');

Contenedor
.register({
    RutasRouter:asFunction(Rutas).singleton(),
    App:asClass(App).singleton(),
    MensajesController:asValue(MensajesController)
})
.register({
    MensajesRoutes:asFunction(MensajesRoutes).singleton()
})

module.exports = Contenedor;