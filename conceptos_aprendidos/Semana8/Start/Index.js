let _Rutas = null;

class Servidor {
    constructor({RutasRouter}){
        _Rutas = RutasRouter
    }
    Arranque(){
        _Rutas.listen(8080, ()=>{
            console.log('Estamos Conectados');
        })
    }
}

module.exports = Servidor;