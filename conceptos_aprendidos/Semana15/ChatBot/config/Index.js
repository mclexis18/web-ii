if ( process.env.NODE_ENV !== "production" )
{
    require("dotenv").config();
}
module.exports = 
{
    APPLICATION_NAME:process.env.APPLICATION_NAME,
    ASISTENTE1:process.env.ASISTENTE1,
    ASISTENTE2:process.env.ASISTENTE2,
    ASISTENTE3:process.env.ASISTENTE3
}