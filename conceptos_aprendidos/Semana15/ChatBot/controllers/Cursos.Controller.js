const Axios = require('axios').default;
const Datos = Axios.get('http://localhost:8080/home/Cursos');

const {Bot} = require('../Chatbot');

const Cursos = async(msg)=> new Promise((resolve, reject)=>{
    if (msg.body === '1')
    {
        resolve(msg.reply('Los cursos disponibles son los siguientes: ⬇️⬇️⬇️⬇️⬇️'));
        Datos.then(p=>
            {
                   p.data.forEach(e=>
                    {
                        resolve(Bot.sendMessage(msg.from, `*${e.id_curso}*. ${e.name_materia}`));
                    }); 
                   resolve(Bot.sendMessage(msg.from, "Para más detalles del curso, escriba el número correspondiente a la opción de interés.\n "+
                    "⚠️No olvide colocar el *0*⚠️ \n"));
            });
            
    }
})

const OpcionesCursos = async(msg)=> new Promise((resolve, reject)=>{
    Datos.then(b=> 
        {
            b.data.forEach(async e=> 
                {
                    if (msg.body === e.id_curso) 
                    {
                        resolve(Bot.sendMessage(msg.from, `*🤩${e.name_materia}🤩* \n Costo: ${e.costo} \n Duración: ${e.duracion} \n Temas: ${e.temas}`));
                        resolve(Bot.sendMessage(msg.from, "✨Gracias por su preferencia.✨\n "+
                                                    " \n"+
                                                    "Si desea inscribirse en nuestro curso ingrese a nuestra pagina web https://cursos-dise.web.app \n"+
                                                    " \n"+
                                                    "↩️ Si desea volver al menú de opciones digite '*'"));;
                    }
                });
                
                //Bot.sendMessage(msg.from ,`Lo sentimos el curso ${msg.body} no esta disponible`);      
        });
       // reject(msg.reply('Lo sentimos😔, el curso no fue encontrado, digite una de nuestras opciones anteriores y disfruta de nuestros cursos🤪😎'));
})


module.exports = {
    Cursos:Cursos,
    OpcionesCursos:OpcionesCursos

};
