const {Saludo} =require('../data');

const SaludoBienvenida = async (msg)=> new Promise((resolve, reject)=>{

    const Datos = Saludo.find(a=> a === msg.body);
    if (!Datos) 
    {
        //reject(console.log('errores'));
    }else 
    {
        console.log('el mensaje fue encontrado');
        resolve(msg.reply( "🤖 *¡Bienvenido a CursosLocos!*, el lugar donde encontrarás los mejores cursos de Diseño Gráfico.👨🏻‍💻  \n "+
        " \n"+
        "Para ayudarle, por favor, escriba el número correspondiente a la opción de interés.\n"+
        " \n"+
        "*1*. Cursos disponibles. 🔎 \n"+
        "*2*. Información de la empresa. ℹ️\n"+
        "*3*. Conversar con un asesor. 🕐 \n"));
      
    }
})



module.exports = SaludoBienvenida;