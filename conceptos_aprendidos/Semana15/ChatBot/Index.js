const {Bot} =require('./Chatbot');
const {Cursos, Saludo, InforEmpresa, Asesor} =require('./controllers');
//const {Saludo} =require('./data');


/**Saludo Inicial*/
    Bot.on('message', async (msg)=>{

        try {
            /**Debemos esperar a entrar al saludo para poder responder el resto */
            await Saludo(msg);
            Bot.on('message', InforEmpresa);
            Bot.on('message', Asesor);
            
                    /**Mientras no entre a la sección de Cursos no podrá ver los Cursos disponibles*/
                     Bot.on('message', async (msg)=>
                     {
                        try {
                              await Cursos.Cursos(msg);
                              Bot.on('message', Cursos.OpcionesCursos);  
                            }catch(error) {
                             // msg.reply(error);
                        }
                     });
            
        } catch (error) 
        {
            console.log(error);          
        }
    });

    

Bot.initialize();
