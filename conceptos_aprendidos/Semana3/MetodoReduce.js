/**UsAMOS EL METODO RECUDE */
/**rEDUCE ES UN METODO  CON EL CUAL cuentan los arreglos */
/**our first example will be a sum */

const Arreglo = [2,38,2,3,54,5]; 
/*If we see specific of reduce we can look that return a callback
with 4 paramer Acumulador, ValorActual, IndexActual y un array */

/**In this line to code we can sum all element in a array */
console.log(Arreglo.reduce((a,b)=> a+b));

/**we can another example with the method reduce */
/**Concated a array multi dimensional  */
/**example  */
const Arreglo2 = [ [234], [567],[8,9,10] ]
let ArregloConvinado = Arreglo2.reduce((Acumulador, Valores)=>
{
    return Acumulador.concat(Valores);

    /**Creamos el arreglo o pasamos el arreglo vacio */
}, []);
console.log(ArregloConvinado);

/**Another example*/

let ArrayArreglo = 
[
    {
        Numero:1
    },
    {
        Numero:2
    },
    {
        Numero:3
    }
]
let Datos = ArrayArreglo.reduce((Acumulador, Valores)=> 
{
      return Acumulador+Valores.Numero;  
/**THIS will be initial value and we can start with the same */
}, 8);
console.log(Datos);

/**we can make another example*/

let Frutas = 
[
    "Platano", "Manzana", "Pera", "Uva", "Sandia"
]
let Conteo = Frutas.reduce((contador, Valores)=>
{
    contador[Valores] += (contador[Frutas]) ;
    return contador;
}, []);

console.log(Conteo);

