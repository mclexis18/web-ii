/**we working to official docs 
 * well First we'll start with conection with mongoose 
 * like We have installed moongose we just call it
*/

/**first example */
/**we'll conect to data_base with localhost */
/**We call it her Module */
/**We must always review that mongoose are installed  */
const Mongoose = require('mongoose');
/**take her paramerts  */
Mongoose.connect('mongodb://localhost/test', {useNewUrlParser: true, 
useUnifiedTopology: true});
/**we can see her conection */
console.log("Conexion exitosa");
/**then we have a pending connection , we now need to get notified 
 * if we connect succesfully 
 */

 /**second example */
const Db = Mongoose.connection;
Db.on('error', console.error.bind(console, 'connection error:'));
Db.once('open', function () 
{
    
})

/**with mongoose, everything is derived from a schema */
/**We definded a new schema */
const Gato = new Mongoose.Schema(
    {
        /**sSchema and array or object are different 
         * because we must make with paramert here
         */
      Nombre: String, Apodo: String  
    });

/**the next step is compiling our schema into a model */
/**umm weel we can say that schema is similar to class 
 * almost like a construct
 */
const GatoConstructor = Mongoose.model('Cat', Gato);

/**Create a new cat */

/**if we make a paramert with a different key we can have mistakes  */
const Toga = new GatoConstructor({Nombre: "Toga", Apodo:"micho"});
console.log(Toga);





