/**Condicionales simples */
const Funcion1 =() => 
{
    const Calificacion = 80; 
    if (Calificacion >=90) 
    {
        console.log(`A`);    
    } else if (Calificacion>= 80 && Calificacion<90) 
    {
        console.log(`B`);
    }
    else if (Calificacion>=70 && Calificacion<80)
    {
        console.log(`C`);
    }
    else 
    {
        console.log(`Reprobado`);
    }  
}
Funcion1();
/**Condicionales Switch */
const Funcion2 =()=> 
{
    const N1= 80, N2 =90, Operacion ="Add";
    switch (Operacion) 
    {
        case "Add":
            console.log(N1+N2);
            break;
        case "Subs": 
            console.log(N1-N2);
        default:
            console.log(`No existe esta operación`);
            break;
    }
}
Funcion2();
/**Arreglos */
const Funcion3 =()=> 
{
    const Frutas = 
    [
        "Manzana",
        "Pera",
        "Guineo",
        function(){
            console.log("Ok");
            return 1;
        } 
    
    ]
    console.log(Frutas[3]());
}
Funcion3();

/**Diferencia entre set and Map */
const FuncionSet =() =>
{
    /**el metodo set cuenta con varios metodos entre los cuales estan */
    const Numeros = new Set();
    /**Agregar */
    Numeros.add(5);
    /*Motrar* al saber que esta es true si no, es faild */
    console.log(Numeros.has(5));
    /**Podemos eliminar con el metodo delete */
    Numeros.add(1999);
    console.log(Numeros);
    Numeros.delete(1999);
    console.log(Numeros);
    /**Mostrar la cantidad de valores */
    console.log(Numeros.size);
    /**Formas de recorrer un set */
    Numeros.add(25);
    /**Normal */
    for(let Valor of Numeros)
    {
        console.log(Valor);
    }
    /**Con sus valores, no hay mucha diferencia siempre y cuando
     * no se quiera metodos de los arreglos para interactuar con el valor 
     * de set
     * 
     */
    for(let valor1 of Numeros.keys())
    {
        console.log(valor1)
    }
    Numeros.add(2020);
  /**Podemos pasarlo a un array  */
  /**Creame un nuevo array desde estos valores */
  const ArrayNuevo = Array.from(Numeros);
  console.log(`Valores: ${ArrayNuevo}`);

  /**Iteracion con el forEach */
  Numeros.forEach((Valores)=> 
  {
    console.log(`Valores por ForEach ${Valores}`)
  })

  /**Otros Metodos para usar  Set */
  
 
} 
FuncionSet();
const valorObjeto = ()=> 
{
    /**Por lo general la cantidad de valores dentro de un objeto
     * es dificil de decifrar con el objeto como tal
     */
    const ObjetoValores = 
    {
        parametro:"Alexis",
        Curso:"6toA"
    }
    /**Para ello usamos la notacion objeto y sus respectivas
     * llaves, de las cuales les vamos a sacar el valor
     * con funcion . legth
     * obviamente pasandole el objeto como parametro
     */
    console.log(Object.keys(ObjetoValores).length);
}
valorObjeto();
/**Ejemplo con map */

const Mapas = ()=> 
{
    let Mapa1 = new Map();
    Mapa1.set("Alexis", "Lopez");
    Mapa1.set("Alfonzo", "Borrero");
    /**El primer parametro que le pasamos es como el alias en un 
     * objeto normal 
     */
    console.log(Mapa1.get("Alfonzo"));
    /**Hasta ahora que puedo apreciar de diferencia entre Map y Set  es que Map agrega un alias se podria decir
     * a diferencia de Set que es mas una variable a secas
     * y tambien en sus diferentes tipos de puntos 
     * ademas los set no nos permiten tener valores repetidos
    */
   /**Para Eliminar es tambien con delete */
   Mapa1.set("Ricardo", "Montaner");
   Mapa1.delete("Alfonzo");
   console.log(Mapa1.get("Alfonzo"));
   /**Podemos recorrer un mapa con el metodo for of */
   for(let[Atributo, Valor] of Mapa1 )
   {
       console.log(`Atributo: ${Atributo}  ValorA: ${Valor} `);
   }
   /**Podemos usar variables de cualquier otro tipo para un map
    * tanto como variables null o undefinded o valores numericos
    */
   Mapa1.set(1999, "Año nacimiento");
   console.log(Mapa1.get(1999));

   /**Vamos a destructurar un poco un Map */
    
   /**Creamos el mapa */
   const MapaD = new Map([
       ["Nombre0", "Alexis"],
       ["Nombre1", "Julisaa"],
       ["Nombre2", "Karen"]
   ]);
   /**Agregamos los atributos o las llaves a una variable */
   const Atributos = [...MapaD.keys()];
   /**Agregamos los valores a una variable */
   const Valores = [...MapaD.values()];

   console.log(`${Atributos}, ${Valores}`);
   /**Puede ser un poco mas ordenado, y serviria mucho para
    * tipo el menu de un restaurant
    */
}
Mapas();

/**Arreglos Asociativos */
const ArreglosA =()=>
{
    let Valores = [555, 422];
    /**Podemos cambiarle el valor accediendo a la posicion */
    Valores[0]="Vamos";
    /**Agregamos un nuevo valor */
    Valores["Abc"]="653"
    console.log(Valores);
    console.log(Valores["Abc"]);
}
ArreglosA();

/**Ciclos */

const CiclosFor =()=> 
{
    const sala = ["Muebles", "Tv", "Escritorio"];
    /**Utilizando ciclo for */
  
    for (let index = 0; index < sala.length; index++)
    {
        console.log( "ciclo for normal" + sala[index]);    
    } 
    /**Utilizando for of */   
    for(let valor of sala)
    {
        console.log("for of " + valor);
    }
    /**Utilizando un for in para el for in es mas recomendable 
     * hacer un tipo objeto
      */
     const personas = [
         {nombre:"Alexis"}, {nombre:"Nardine"}
        ]
      for (const Datos in personas)
      {
          console.log(` For in ${personas[Datos].nombre}`);
      }  
}
CiclosFor();

const CiclosWhile = ()=> 
{
    const personas = [
        {nombre:"Alexis"}, {nombre:"Nardine"}
       ];
   let Iterador=0;    
   do {
       console.log("do-while" +" " + personas[Iterador].nombre);
       Iterador++;
   } while (Iterador<personas.length);   
   let Iterador2=0;
   while (Iterador2<personas.length) {
    console.log("while" +" " + personas[Iterador2].nombre);
    Iterador2++;
   }
}
CiclosWhile();

const Clases =()=> 
{
    console.log("Aqui comienzan las clases");
    /**Podemos definir las clases como objetos */
    const Alumno = 
    {
        Nombre:"Alexis",
        Apellido:"Lopez",
        EsEstudiante:true,
        NombreCompleto()
        {
            return `${this.Nombre} ${this.Apellido}`
        }
    }
    console.log(Alumno.Nombre);
    console.log(Alumno.Apellido);
    console.log(Alumno.NombreCompleto());
    /**lo recorremos */
    for(let Valores in Alumno)
    {
        console.log(`${Valores} ${Alumno[Valores]}`)
    }

    /**Podemos tambien crear una clase de la siguiente
     * manera
     */
    console.log("Aqui comienzan las clases")
    class Persona2
    {
        constructor(Nombre)
        {
            this.Nombre = Nombre;
        }
        getObtenNombre()
        {
            return this.Nombre;
        }
    }
    const NuevaPersona = new Persona2("Alexis Lopez");
    const NombrePersona = NuevaPersona.getObtenNombre();
    console.log(NombrePersona);
    /**Este es un constructor con una funcion */
    function DatosPersonas (Nombre, Apellido, Cedula)
    {
        this.Nombre = Nombre;
        this.Apellido = Apellido;
        this.Cedula = Cedula;
    }
    /**Creamos la instancia */
    let DatosAlexis = new DatosPersonas("Alexis", "Ricardo", 13456566);
    console.log(DatosAlexis);

}
Clases();


const Funciones =()=> 
{
    /**Estas son conocidas como las funciones anonimas */
   function Saludando(NombrePersona) 
   {
      console.log(` Saludos ${NombrePersona}`);      
   }   
   Saludando("Alexis");
   /**estas funciones se pueden pasar dentro
    * de otras funciones
    */
   function LlamarSaludo(Saludo, Quien)
   {
        Saludo(Quien);
   }
   LlamarSaludo(Saludando, "Alexis López");
   /**Ahora veremos los array funcions o funciones
    * flechas como tambien se las conoce, aunque ya 
    * las eh usado en todo el documento :c, veremos una definicion formal
    */
   /**Podemos hacer una constante igual a una funcion */
   const SaludameOtravez = function (DimeHola)
   {
        console.log(`Te digo hola ${DimeHola}`);   
   }
   SaludameOtravez("Nardine");
   /**O podemos definirla como la funcion flecha
    * y de esta manera se ve el codigo mas limpio 
    * bueno, aunque hay mucho comentario en este jajajja
    */
   const SaludameDenuevo =(Saludame)=>{console.log(`Te saludo ${Saludame}`)};
   SaludameDenuevo("Karen");
   /**Dato inportante las funciones flechas no pueden ser llamadas
    * antes de su inicializacion, si queremos hacer constructores, no es 
    * recomendable las array functions, por que el contexto
    * es un escalon mas del que se encuentra actualmente 
    */

}
Funciones();
