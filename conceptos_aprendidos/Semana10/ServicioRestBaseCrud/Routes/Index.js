const {Router} = require('express');



module.exports = function ({UsuariosRoutes, UserFreeRoutes}) {

    const Api =Router();
    Api.use('/Paga', UsuariosRoutes);
    Api.use('/Free', UserFreeRoutes);
    return Api;
    
}