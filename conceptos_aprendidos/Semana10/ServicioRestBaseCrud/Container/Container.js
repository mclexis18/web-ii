const {createContainer, asClass, asValue, asFunction} = require('awilix');

const {UserControllers, BaseControllers, UserFreeControllers} =require('../Controllers');

const Rutas =require('../Routes');

const {UsuariosRoutes, UserFreeRoutes} = require('../Routes/Index.Routes');

const NameA = require('../Values/Datos');

const Container = createContainer();

Container.register({

    UserControllers:asClass(UserControllers).singleton(),
    BaseControllers:asClass(BaseControllers).singleton(),
    Rutas:asFunction(Rutas).singleton(),
    UsuariosRoutes:asFunction(UsuariosRoutes).singleton(),
    UserFreeControllers:asClass(UserFreeControllers).singleton(),
    UserFreeRoutes:asFunction(UserFreeRoutes).singleton(),
    NameA:asValue(NameA)


});
module.exports = Container;
