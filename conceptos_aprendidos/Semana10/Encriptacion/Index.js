const Express = require('express');
const {hash, hashSync, genSaltSync, genSalt, compareSync, compare} = require('bcryptjs');
const App = Express();
const Datos = require('./ClasePrueba');

/**we are two way to make encyptation asyn and sync  */

/**way sync we are encypta the password*/
App.use(Express.json());
App.get('/password', (req, res)=>{

    const {password} = req.body;
    const Password =  password; 
    /**Dependiendo de la Interacción sera mas segura */
    let Salt = genSaltSync(10);
    let Hash = hashSync(Password, Salt); 
    let Comparando = compareSync(Password, Hash);
    res.send(
        {
            Password:Password,
            PasswordEncrypt: Hash,
            Comparacion:Comparando 
        }
    );    
});

/**way async we are encypta the password*/
App.get('/Name', async(req, res)=>{
    
    const {name} = req.body;
    const Name = name;
    const Salt = await genSalt(10);
    const NameEncrypt = await hash(Name, Salt);
    const Comparando = await compare(Name, NameEncrypt)
    res.send(
        {
            NombreNormal:Name,
            NombreEncriptado: NameEncrypt,
            Comparacion:Comparando
        }
    )

});

App.get('/Parametros/:Alexis', Datos.Index);

App.listen(3000, ()=>{
   console.log('We are wake up');
});

Datos.Saludo;

/** */