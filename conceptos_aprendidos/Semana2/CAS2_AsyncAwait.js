/**we can work async with sincrono in the same function 
 * this's thanks word await , we can used async await with promise 
 * for example ummm ewe avoid use then 
 */

const PromesaNumeros =(Numero)=> new Promise((resuelve, rechaza)=>{
    setTimeout(()=>{
        if (Numero>9) {
               resuelve("that's true great"); 
        }
    }, 500);
});
/**how sew in the result we don't need use .then to work this functiont */
async function Asincronia() 
{  
    console.log("we're in proccess");
    /**that function wait to PromesaNumeros */
    const Called = await PromesaNumeros(10);
    console.log(Called);
}

Asincronia();

/**Another examplo so. I mean that async await work to make easier promises */
/**thuis example is with  paramert ummm well I think so jaaja */
const Comidas = 
[
    {
        Id:1,
        Comida:"Patacon con pescado"
    },
    {
        Id:2,
        Comida:"Arroz con ceviche"
    }
]

const BusarComidaporId =(IdComida)=> new Promise((resuelve, rechaza)=>{

        const Micomida = Comidas.find((Valor)=> Valor.Id === IdComida);
        if (!Micomida)
        {
            rechaza('Esa comida no esta agregada');        
        }
        resuelve(Micomida.Comida);

}); 
async function LlamaComida()
{
    console.log('Ejecutando');
    /**waiting */
    const ValorComida = await BusarComidaporId(5);
    console.log(ValorComida);   
}
/**how we can look the funcion LlamaComida can it make use to function or method cacth  */
LlamaComida().catch((ValoresError)=>{
    console.log(ValoresError);
});

/**we can use when we want that load all files in an app that function
 * for example if don't get  that must load everything so then we can make mistakes 
  */