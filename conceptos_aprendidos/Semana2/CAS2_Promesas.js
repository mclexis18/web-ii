/**like in the real life promises didn't always words and this have a lof of method */

/**example basic */
/**Our fist promise it's about find if a value that's rigth or not */
const PromesaBasica = new Promise((resuelve, ejecuta)=>
{
    let Valor = 1;
    setTimeout(function()
    {
        if (Valor>1) {
            resuelve(`Funciono el engaño`); 
        }
    }, 500);
    ejecuta(`Fallo el proceso`);
});
PromesaBasica.then(Funciono=>
{
    console.log(Funciono);
}).catch(Errores=>
{
    console.log(Errores);
});


/**Second Ejemplo **/
/**Also we can have promise with values into function */
const Notas =
[
    {
        Id:1,
        MateriaNombre:'Calculo',
        Calificacion:1
    },
    {
        Id:2,
        MateriaNombre:'Programación',
        Calificacion:7
    },
    {
        Id:1,
        MateriaNombre:'Estadistica',
        Calificacion:9
    }
]

/**we can also make promise with paramer  */
const VerificarCalificaciones =(Calificaciones)=> new Promise((resuelve, rechaza)=>
{   
    let NotaFinal =0;
    for(let Valores in Calificaciones)
    {
       NotaFinal = NotaFinal + Calificaciones[Valores].Calificacion;
    }
    const Promedio = Math.trunc(NotaFinal/Calificaciones.length);
    if (Promedio>6)
    {
        resuelve(Promedio);
    } 
    const Errores = new Error();
    Errores.message = "Error absoluto"
    rechaza(Errores);
});
VerificarCalificaciones(Notas).then((ValoresAprobado)=>
{
    console.log(`Aprobo su promedio es ${ValoresAprobado}`);
}).catch((ValoresRechazo)=>{
    console.log(`Fatal ${ValoresRechazo} usted a reprobado`);
});


/**Example three  we going to make another example with something promise in the real life*/
/**for example  some guys want to promised something amoung they and we wante know if they can doing it   */

const Promesas = 
[
    {
        Id:1,
        Promesa:"Hacer Ejercicio",
        estado:true,
        IdPersona:3
    },
    {
        Id:2,
        Promesa:"Estudiar Ingles",
        estado:false,
        IdPersona:1
    },
    {
        Id:3,
        Promesa:true,
        Promesa:"Correr en las mañanas",
        IdPersona:2
    }
]
const Personas =
[
    {
        Id:1,
        Nombre:"Alexis López",
        Edad:22
    },
    {
        Id:2,
        Nombre:"Karen Santana",
        Edad:20 
    },
    {
        Id:3,
        Nombre:"Nardine Alsa",
        Edad:22
    }
]
const VerificarPromesa =(PersonaId) => new Promise((resuelve, rechaza)=>{

    const Promesa = Promesas.find((Encuentralo)=> Encuentralo.IdPersona === PersonaId); 
    if (Promesa.estado === false) {
        rechaza(`La persona ${Promesa.IdPersona} prometio ${Promesa.Promesa} y no lo cumplio `);
    }
    resuelve(`la persona ${Promesa.IdPersona} prometio ${Promesa.Promesa} y si cumplio `);

}).then((Resolviendo)=>{
    console.log(`Felicidades ${Resolviendo}`);
}).catch((AgarrandoError)=>{
    console.log(`Nha eres bago ${AgarrandoError}`);
});

VerificarPromesa(2);



