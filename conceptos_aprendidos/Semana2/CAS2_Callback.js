/**I'll talk abaout callback, promise and async await */

/**Callback's function that called yourselft In my opinion is similar to recursividad */

/**Callback basic example*/

/**first we create a normal function, then.
 * well on this case it's a array function
 */
const CrearAvatar =(Avatar)=>
{
    console.log(`Este es mi Avatar, su nombre es ${Avatar}`);
}
/**Then we Create another function with a parame callaback 
 * that paramer be called yourselft with a paramer 
 * that paramer we create it inside to second function
*/
const LlamarAvatar =(AvatarCallback)=>
{
    //this is a paramer
    var Nombre = 'Alexis'
    //here we're called callback 
    AvatarCallback(Nombre);
}
/**alreadi we called second function with a paramer 
 * exactly that paramer is the first function
 */
//LlamarAvatar(CrearAvatar);

/**second example 
 * we go to use a example with un delay It seems to me 
 * a good example to practice  
*/
const EjecutaPrimero= ()=> 
{
    setTimeout(function(){
        console.log(`Numero1`);
    }, 600)
}
const EjecutarSegundo =()=> 
{
    console.log(`Numero2`)
}
EjecutaPrimero();
EjecutarSegundo();

/**answers will be something like 
 * Numero2, then Numero2 because we are working with 
 * setTimeout o tiempo fuera with this we can get it better how words that function callback 
 */

 /**Example 2 */
 const HaciendoTarea =(Materia, CallbackAnswer)=>
 {
    console.log(`I'm doing my homework of ${Materia}`);
    CallbackAnswer();
 }
/**In this example we can see that Did't finalize homework without
 * Start it
 */
 HaciendoTarea('Programacion', (Finalizo)=>{
     setTimeout(function()
     {
         console.log(`I finised my homework`)
     }, 700);
 } );

 /**like this we can avoid make mistakes or we've more code */
 /**Example 3 */
 
/**Another example with basic operation  */
console.log("Se esta ejecutando");
const ValoresDevueltos =(a, b, MyCallback)=>
{
    MyCallback(a+b);
};

ValoresDevueltos(6,7, (Proceso)=>{
    console.log(Proceso);
});

/**Another explaint that we can doing are something like
 * la funcion que resvie el callback se va a encadenar con la otra funcion
 * que la va a llamar, suena complicado, but vamos a tratar de entenderlo
 */

 /**example four */
 const Valores = 
 {
     Nombre:"Alexis",
     Apellido:"Lopez",
     Materia:"Programación"
 }
 /**we gonna changes hir name */
 const CambioDeNombre=(Datos, CambioCallback)=>{
    DatosCambiados = Datos;
    DatosCambiados.Nombre ="Toronto";
    CambioCallback(DatosCambiados);
 }
 const CambioApellido =(NombreNuevo)=>
 {
    ApellidoNuevo = NombreNuevo;
    ApellidoNuevo.Apellido = "Azteca"; 
    console.log(`El cambio de nombre es: ${NombreNuevo.Nombre}, y su nuevo apellido es ${ApellidoNuevo.Apellido}`);
 }
 CambioDeNombre(Valores, CambioApellido);

 /**we show everything changes  */
 console.log(Valores);