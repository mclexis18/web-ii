/**Middelwares cumples with the next paramer 
 * ejecutr wathever code
 * it make changes
 * finaliza ciclo de solicitud
 *para usarlo debemos utilizar la libreria express
*/

const Express = require('express');
const App = Express();

/**req= Argumenta solicitus HTTP  
 * res= Argumenta respuesta 
 * next Devolucion de la llamada a funcion 
 */

 /**el middelware vienen siendo los parametros que pasamos 
  * como el req, res y el next, req solicitamos, res, respondemos 
  */

  /**Esta funcion se mostrara cada que hagamos un llamado
   * a la aplicacion es decir que cuando se ejecuteel 
   * app.listen
   */
 const MuestraNombre = (req, res, next)=>
 {
     console.log('Mostrando Nombre');
     next();
 }
 
App.use(MuestraNombre);

const HoraActual = (req, res, next)=> 
{
    req.Datos = {Nombre:'Alexis'}
    next();
}
App.use(HoraActual);

  App.get('/', (req, res, next)=>
{
    var responseText = 'Hello world';
    responseText += 'Requiriendo tiempo' + req.Datos.Nombre + '';
    res.send(responseText);
    res.end();
});



const Alumnos = (req, res, next)=> 
{
    req.Alumno = 
    {
        Nombre:'Alexis López',
        Calificacion: 7,
        Curso:'6to B'
    }
    next();
}

App.use(Alumnos)

App.get('/Datos', (req, res)=>
{
    /**Se llama el objeto que se quiere agarrar, mas no su funcion   */
    const Datos = req.Alumno;
    res.send(Datos);
    res.end();
});

App.listen(8080, ()=> console.log('Corriendo'));

/**Ejercicio 2 */

/**en resumidas cuentas un middelwarer viene siendo las llamadas y peticiones
 * que podemos hacer con res, req y seguir ejecutando con next, hay mas tipos de middelwares, que son 
 * para manejor de errors, direccionamiento, aplicacion, etc
 * https://expressjs.com/es/guide/using-middleware.html
 */

