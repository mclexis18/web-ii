/**Es un principio de estructura para las buenas practicas en base 
 * a los patrones de diseño, si tenemos varios atributos y queremos cambiarlo en diferentes partes
 * se vuelve complejo , entonces, debemos llamar el objeto como tal
 * que ya esta creado, y a partir de ahi podemos acceder a los datos que este mismo esta implementando 
 * y todo eso se va a definir dentro de un container  
 */