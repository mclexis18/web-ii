/**In that ocasion I'll talk about value to entornos, las valiables de entornos son informacion sencible que debe 
 * de ir en una rama de arriba  a la del proyecto Principal, para ella se necesita el paquete dotenv
 */
/**in tha practice I try connect to a data base and load text and then I'LL try shared that dates for a appi */
const Mongoose = require('mongoose');
const Express = require('express');
const {PORT, CONEXION} = require('./Config');
const Cors = require('cors');
const {Data} = require('./Models');
const Fs = require('fs');
const Server = Express();
const Home = Fs.readFileSync('./Public/Index.html');


(async ()=>
{

    await Mongoose.connect(CONEXION, {useNewUrlParser:true, useUnifiedTopology:true}).then(()=>
    {
    console.log(`La conexión a la base de datos fue exitosa`);
    });
 

    Server.use(Cors()).use(Express.json());

    await Server.listen(PORT, ()=>
    {
        console.log(`Conexion exitosa en el puerto: ${PORT}`);
    });


})();
Server.get('/', (req,res)=>
{
    res.write(Home);
    res.end();
});


