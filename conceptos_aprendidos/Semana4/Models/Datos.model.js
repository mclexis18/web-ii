const Mongoose = require('mongoose'); 
const {Schema} = Mongoose;

const DatosPersonales = 
new Schema(
{
    Nombre:{type:String},
    Apellido:{Type:String}
});

module.exports = Mongoose.model("Data", DatosPersonales);