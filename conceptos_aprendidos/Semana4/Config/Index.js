if ( process.env.NODE_ENV !== "production" )
{
    require("dotenv").config();
}

module.exports=
{
    CONEXION:process.env.CONEXION,
    PORT:process.env.PORT
}
