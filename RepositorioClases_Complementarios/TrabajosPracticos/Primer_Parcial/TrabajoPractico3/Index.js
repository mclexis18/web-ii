/**Trabajo autonomo 3  */
/**Crear un documento JavaScript que pueda ser ejecutado con Node.js 
 * y aplique los scraping y cron job, almacenando los datos minados en una base
 * de datos no relacionados 
 */
const Mongoose = require("mongoose");
const Axios = require("axios").default;
const Cheerio = require("cheerio");
const {CADENA_CONEXION} = require('./config');
const CronJob = require("node-cron");
const {Cursos} = require('./models');
Mongoose.connect(CADENA_CONEXION, {useNewUrlParser:true, useUnifiedTopology:true});

/**Los vamos a actualizar cada 50  segundos */
CronJob.schedule('50 * * * * *', 
async ()=> 
{
    const Html = await Axios.get("https://www.domestika.org/es");
    const $ = Cheerio.load(Html.data);
    const CursosN = $(".course-item__details");
    
    let ListaCursos = []
    CursosN.each((index, element)=>
    {

        const Curso = 
        {
            Descripcion:$(element).children(".course-item__title").text(),
            Docente:$(element).children(".course-item__teacher").text()
        }
        ListaCursos=[...ListaCursos, Curso]
       
    });
    console.log(ListaCursos);
    Cursos.create(ListaCursos);  
}

);


