const Mongoose = require("mongoose");
const {Schema} = Mongoose;
const Cursos = new Schema 
(
    {
        Descripcion:{ type:String },
        Docente:{ type:String}
    }
    ,
    {timestamps: {createAt:true, updatedAt:true}}
);
module.exports = Mongoose.model("Cursos", Cursos); 

