/**1. Crear Una funcion que reciba N como parametros
 * y genere una tabla de multiplicar por consola utilizando 
 * recursividad
 */
const FuncionMutiplicar =(ValorN, Valores)=>
{
    if (ValorN<=0) return
     let ValoresO = `${Valores} * ${ValorN} = ${ValorN*Valores}`;
    console.log(ValoresO);
    return FuncionMutiplicar(ValorN-1, Valores); 

}
FuncionMutiplicar(12, 10);

/** 2. Crear un Objeto Mascota que tenga como parámetros 
 * Nombre, Edad y Color
 */
const Mascota= 
{
    Nombre:"Driff",
    Edad: 10,
    Color:"black"
}
/**3. Definir un arreglo con sus comidas favoritas */
const ComidasFavoritas1= 
[
    "Ceviche", "Tallarin", "Encebollado"
]
/**Forma diferente de hacer el ejercicio 3 */
const ComidasFavoritas2 = 
[
    {Plato:"Camaron"}, {Plato:"Carne"}, {Plato:"Pulpo"}
]
/**4. Recorrer el arreglo definido en la opcion anterior y mostrarlo aplicando
 * un do-while
 */
let Iteraccion = 0;
do {
    console.log(ComidasFavoritas1[Iteraccion]);
    Iteraccion++;
} while (Iteraccion< ComidasFavoritas1.length);


/**Forma diferente de recorrer el ejercicio3 */

let Iteraccion2 = 0;
do {
    console.log( `Iteraccion con objetos en arreglo ${ComidasFavoritas2[Iteraccion2].Plato}`);
    Iteraccion2++;
} while (Iteraccion2<ComidasFavoritas2.length);

/**Crear una funcion flecha que reciba un elemento del arreglo
 * comidas favoritas y lo devuelva en mayuscula 
 */
const Devuelve =(valor)=>{console.log(valor)}
Devuelve(ComidasFavoritas1[1].toUpperCase());
