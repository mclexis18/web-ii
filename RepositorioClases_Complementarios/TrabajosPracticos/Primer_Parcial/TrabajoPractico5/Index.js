const Container = require('./src/Startup/Container');
const {connect} = require('mongoose');
const Servidor = Container.resolve('App');


const {CADENA_CONEXION} = Container.resolve('VariablesEntorno');

connect(CADENA_CONEXION, {useNewUrlParser:true, useFindAndModify:true})
.then(()=> Servidor.Start())
.catch(console.log);