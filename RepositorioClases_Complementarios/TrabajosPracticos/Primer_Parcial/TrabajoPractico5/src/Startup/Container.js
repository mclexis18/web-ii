const {createContainer, asClass, asFunction, asValue} =require('awilix');

const VariablesEntorno = require('../Config');
/**Routes */
const {UsuarioRouter, CursosRouter, InstructorRouter, Api} =require('../Routes/Index.Routes');

/**valores */
const Router =require('../Routes');

/**Clase */
const App = require('.');

/**Modelos */
const {Cursos, Instructor, Usuarios} = require('../Models');

/**Repositorios */

const {CursosRepository, UsuariosRepository, InstructorRepository} = require('../Repositories');

/**Service */

const {UsuariosService, InstructorService, CursosService} = require('../Services');

/**Controllers */

const {CursosController, UsuariosController, InstructorController} = require('../Controllers');


const Contenedor = createContainer();

Contenedor
    
    .register(
        {
            VariablesEntorno:asValue(VariablesEntorno),
            App:asClass(App).singleton(),
            Router:asFunction(Router).singleton()

        })
        /**Rutas */
    .register(
        {
            UsuarioRouter:asFunction(UsuarioRouter).singleton(),
            CursosRouter:asFunction(CursosRouter).singleton(),
            InstructorRouter:asFunction(InstructorRouter).singleton(),
            Api:asClass(Api).singleton()

        })
        /**Modelos */
    .register(
        {
            Cursos:asValue(Cursos),
            Instructor:asValue(Instructor),
            Usuarios:asValue(Usuarios)
        })
        /**Repositorios */
    .register(
        {
            CursosRepository:asClass(CursosRepository.bind(CursosRepository)).singleton(),
            InstructorRepository:asClass(InstructorRepository.bind(InstructorRepository)).singleton(),
            UsuariosRepository:asClass(UsuariosRepository.bind(UsuariosRepository)).singleton()
        })
        /**Services */
    .register(
        {
            UsuariosService:asClass(UsuariosService).singleton(),
            CursosService:asClass(CursosService).singleton(),
            InstructorService:asClass(InstructorService).singleton()
        })
        /**Controller */
    .register(
        {
            CursosController:asClass(CursosController).singleton(),
            UsuariosController:asClass(UsuariosController.bind(UsuariosController)).singleton(),
            InstructorController:asClass(InstructorController).singleton()

        })

/**Nota:we must care when exports because can be asfunction or asvalue, we're must sure */

module.exports = Contenedor;