const Express = require('express');

let _Express = null;
let _VariablesEntorno = null;

class Servidor 
{

    constructor({Router, VariablesEntorno}){
        
        _VariablesEntorno = VariablesEntorno;
        _Express = Express().use(Router);

    }

    Start ()
    {
        return new Promise(resolve=>{

            _Express.listen(_VariablesEntorno.PORT, ()=>{

                console.log(`Servidor corriendo por el puerto ${_VariablesEntorno.PORT}`);

                resolve();

            })

        });

    }

}

module.exports = Servidor;