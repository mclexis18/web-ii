const Mongoose =require('mongoose');
const {Schema} = Mongoose;

const DatosUsuario = new Schema({
    
    Nombres:{type:String, require:true},
    Apellidos:{type:String, require:true},
    Cedula:{type:String, require:true}
}); 

module.exports = Mongoose.model('DatosUsuario', DatosUsuario);