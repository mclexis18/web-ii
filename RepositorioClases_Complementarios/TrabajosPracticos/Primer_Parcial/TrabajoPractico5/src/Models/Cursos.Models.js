const Mongoose = require('mongoose');
const {Schema} = Mongoose;

const CursoSchema = new Schema({

    Id_Curso: {type:String, require:true},
    Name_Materia: {type:String, require:true},
    Duracion:{type:String, require:true},
    Costo:{type:String, require:true},
    Temas:{type:String, require:true}

});

module.exports = Mongoose.model("Cursos", CursoSchema);