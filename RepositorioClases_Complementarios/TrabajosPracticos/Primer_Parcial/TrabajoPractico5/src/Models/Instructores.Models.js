const Mongoose = require('mongoose');
const {Schema} = Mongoose;

const InstructorSchema = new Schema({

    Identificacion:{type:String, require:true, max:10},
    Nombre:{type:String, require:true},
    Direccion:{type:String, require:true},
    Telefono: {type:String, require:true, max:10}

});

module.exports = Mongoose.model('Instructor', InstructorSchema);