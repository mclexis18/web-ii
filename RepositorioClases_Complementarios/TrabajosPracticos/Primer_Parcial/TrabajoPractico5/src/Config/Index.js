if ( process.env.NODE_ENV !== "production" )
{
    require("dotenv").config();
}

module.exports=
{
    PORT:process.env.PORT,
    CADENA_CONEXION:process.env.CADENA_CONEXION
}
