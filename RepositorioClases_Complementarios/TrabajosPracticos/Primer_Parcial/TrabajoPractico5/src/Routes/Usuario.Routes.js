const {Router} =require('express');

module.exports = function ({UsuariosController}){

    const Rutas = Router();
    Rutas.get('/:UserId', UsuariosController.Get);
    Rutas.get('', UsuariosController.GetAll);
    Rutas.post('', UsuariosController.Create);
    Rutas.patch('/:UserId', UsuariosController.Update);
    Rutas.delete('/:UserId', UsuariosController.Delete);
 
    return Rutas;

    
}