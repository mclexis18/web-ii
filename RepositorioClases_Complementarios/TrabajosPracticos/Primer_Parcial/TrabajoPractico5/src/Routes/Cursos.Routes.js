const {Router} =require('express');

module.exports = function ({CursosController}) {

    const router = Router();
    
    router.get('/:CursoId', CursosController.Get);
    router.get('', CursosController.GetAll);
    router.post('', CursosController.Create);
    router.patch('/:CursoId', CursosController.Update);
    router.delete('/:CursoId', CursosController.Delete);
    return router;

    
}