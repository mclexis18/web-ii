const {Router} =require('express');

module.exports = function ({InstructorController}){

    const route = Router();
    
    route.get('/:InstructorId', InstructorController.Get);
    route.get('', InstructorController.GetAll);
    route.post('', InstructorController.Create);
    route.patch('/:InstructorId', InstructorController.Update);
    route.delete('/:InstructorId', InstructorController.Delete);


    return route;
    
}