const Express = require('express');
const Cors = require('cors');
const Helmet = require('helmet');
const Compression = require('compression');


module.exports= function ({CursosRouter, InstructorRouter, UsuarioRouter}){

    const RutasCursos = Express.Router();
    const App = Express.Router();

    RutasCursos.
    use(Express.json())
    .use(Cors())
    .use(Helmet())
    .use(Compression());

 
    RutasCursos.use('/Cursos', CursosRouter);
    RutasCursos.use('/Instructores', InstructorRouter);
    RutasCursos.use('/Usuarios', UsuarioRouter);
    

    App.use('/v1/aut5', RutasCursos);
   

    return App

    /**Localhost:Puerto/v1/aut5/Home */
}