const BaseRepository = require('./Base.Repository');
let _Usuarios = null

class UsuariosRepository extends BaseRepository{

    constructor({Usuarios}){

        super(Usuarios);
        _Usuarios = Usuarios;

    }

}


module.exports = UsuariosRepository;