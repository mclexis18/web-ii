const BaseRepository = require('./Base.Repository');

let _Cursos = null;

class CursosRepository extends BaseRepository {

    constructor({Cursos}){

        super(Cursos);
        _Cursos = Cursos;

    }

}

module.exports = CursosRepository;