class BaseRepository{

    constructor(model){
        this.model = model;
    }

    /**Id */
    async Get(id){
        return await this.model.findById(id);
    }

    /**All */
    async GetAll(){

        return await this.model.find();

    }
    /**Create can be body */
    async Create(entity){

        return await this.model.create(entity);

    }
    /**Update by Id  */
    async Update(id, entity){

        return await this.model.findByIdAndUpdate(id, entity, {new:true});

    }

    /**Delebe by Id */
    async Delete(id){

        return await this.model.findByIdAndDelete(id);

    }

}

module.exports = BaseRepository;