const BaseRepository = require('./Base.Repository');

let _Instructor = null;

class InstructorRepository extends BaseRepository{

    constructor({Instructor}){
        
        super(Instructor);
        _Instructor = Instructor;

    }

}

module.exports = InstructorRepository;