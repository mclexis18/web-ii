
let _UsuariosService = null;
class UsuariosController  {

    constructor({UsuariosService}){

        _UsuariosService = UsuariosService;

    }

     //get
     async Get(req, res){

        const {UserId} = req.params;
        const Contenido = await _UsuariosService.Get(UserId);
        return res.send(Contenido);

    }
    //GetAll
    async GetAll(req, res){

        const Contenido = await _UsuariosService.GetAll();
        return res.send(Contenido);

    }
    /**Create */
    
    async Create(req, res){

        const {body} = req;
        const ContenidoCreado = await _UsuariosService.Create(body);
        return res.send(ContenidoCreado);
        
    }

    /**Update */
    async Update(req, res){

        const {UserId} = req.params;
        const {body} = req;
        const ContenidoActualizado = await _UsuariosService.Update(UserId, body);
        return res.send(ContenidoActualizado);

    }
    async Delete(req, res){

        const {UserId} = req.params;
        const ContenidoEliminado = await _UsuariosService.Delete(UserId);
        return res.send({
            Contenido:'Ah sido Eliminado'
        });

    }


}



module.exports = UsuariosController;