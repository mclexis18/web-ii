

let _CursosService = null;

class CursosController {

    constructor({CursosService}){

        _CursosService = CursosService;

    }

     //get
     async Get(req, res){

        const {CursoId} = req.params;
        const Contenido = await _CursosService.Get(CursoId);
        return res.send(Contenido);

    }
    //GetAll
    async GetAll(req, res){

        const Contenido = await _CursosService.GetAll();
        return res.send(Contenido);

    }
    /**Create */
    
    async Create(req, res){

        const {body} = req;
        const ContenidoCreado = await _CursosService.Create(body);
        return res.send(ContenidoCreado);
        
    }

    /**Update */
    async Update(req, res){

        const {CursoId} = req.params;
        const {body} = req;
        const ContenidoActualizado = await _CursosService.Update(CursoId, body);
        return res.send(ContenidoActualizado);

    }
    async Delete(req, res){

        const {CursoId} = req.params;
        const ContenidoEliminado = await _CursosService.Delete(CursoId);
        return res.send({
            Contenido:'Ah sido Eliminado'
        });

    }

}

module.exports = CursosController;