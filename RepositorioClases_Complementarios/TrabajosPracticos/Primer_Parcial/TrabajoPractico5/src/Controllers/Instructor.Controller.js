
let _InstructorService = null;

class InstructorController {

    constructor({InstructorService}){

        _InstructorService = InstructorService;

    }

     //get
     async Get(req, res){

        const {InstructorId} = req.params;
        const Contenido = await _InstructorService.Get(InstructorId);
        return res.send(Contenido);

    }
    //GetAll
    async GetAll(req, res){

        const Contenido = await _InstructorService.GetAll();
        return res.send(Contenido);

    }
    /**Create */
    
    async Create(req, res){

        const {body} = req;
        const ContenidoCreado = await _InstructorService.Create(body);
        return res.send(ContenidoCreado);
        
    }

    /**Update */
    async Update(req, res){

        const {InstructorId} = req.params;
        const {body} = req;
        const ContenidoActualizado = await _InstructorService.Update(InstructorId, body);
        return res.send(ContenidoActualizado);

    }
    async Delete(req, res){

        const {InstructorId} = req.params;
        const ContenidoEliminado = await _InstructorService.Delete(InstructorId);
        return res.send({
            Contenido:'Ah sido Eliminado'
        });

    }



}

module.exports = InstructorController;