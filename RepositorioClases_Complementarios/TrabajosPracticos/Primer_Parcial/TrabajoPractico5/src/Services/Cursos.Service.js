const BaseService = require('./Base.Service');

let _CursosRepository = null;

class CursosService extends BaseService {

    constructor({CursosRepository}){

        super(CursosRepository);
        _CursosRepository = CursosRepository;

    }

}

module.exports = CursosService;