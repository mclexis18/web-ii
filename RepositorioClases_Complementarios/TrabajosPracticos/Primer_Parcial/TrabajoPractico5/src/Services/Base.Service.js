
class BaseService{

    constructor(repository){
        this.repository = repository;
    }

    async Get(id){

        /**Realizamos un tipo de validación */
        if (!id)
        {

            const error = new Error();
            error.status = 400;
            error.message = "ID Obligatorio you know ";
            throw error;

        }
        else{
            const DatosObtenido = await this.repository.Get(id);
            if (!DatosObtenido) 
            {
                
                const error = new Error();
                error.status = 400;
                error.message = "Datos No encontrados -.-";
                throw error;
    
            }
            return DatosObtenido;



        }
        /**Accedemos a los metodos del repositorio */
    }

    /**Obtenemos todos los datos */
    async GetAll(){

        return await this.repository.GetAll();

    }

    /**Creamos recordando que los metodos pertenecen al repositorio */
    async Create(entity){

        return await this.repository.Create(entity);

    }

    /**Actualizamos mediante el Id y la entidad */
    async Update(id, entity){

        if (!id)
        {

            const error = new Error();
            error.status = 400;
            error.message = "Para Actualizar necesitamos el Id -.-";
            throw error;

        }
        return await this.repository.Update(id, entity);

    }

    async Delete(id){
        
        if (!id) {
            
            const error = new Error();
            error.status = 400;
            error.message = "ID Obligatorio para eliminar -.-";
            throw error;

        }
        return await this.repository.Delete(id);

    }


}

module.exports = BaseService;