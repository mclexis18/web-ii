const BaseService = require('./Base.Service');

let _InstructorRepository = null;

class InstructorService extends BaseService {

    constructor({InstructorRepository}){

        super(InstructorRepository);
        _InstructorRepository = InstructorRepository;

    }

}

module.exports = InstructorService;