const BaseService = require('./Base.Service');

let _UsuariosRepository = null

class UsuariosService extends BaseService {
    
    constructor({UsuariosRepository}){
        
        super(UsuariosRepository);
        _UsuariosRepository = UsuariosRepository

    }

}

module.exports = UsuariosService;