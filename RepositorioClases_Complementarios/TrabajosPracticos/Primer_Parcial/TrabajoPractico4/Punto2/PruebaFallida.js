const Mongoose = require('mongoose');
const Express =require('express');
const App = Express();
const {CADENA_CONEXION, PORT} = require('./config');
const {Noticias} = require('./Models');
App.use(Express.json);

/**Conectamos a la base de datos*/
(async ()=> 
{

    await Mongoose.connect(CADENA_CONEXION, {useNewUrlParser:true, useUnifiedTopology:true}).then(()=>
    {
    console.log('conexion exitosa'); 
    });
    await App.get('/Documentos', (req, res)=>
    {
        Noticias.find({}, (error, docs)=>
        {
            res.send(
                {
                    docs
                });
            
        });
       
    }  
    );

}) ();
App.listen(PORT, ()=>
{
    console.log(`App corriendo en el puerto ${PORT}`);
})


