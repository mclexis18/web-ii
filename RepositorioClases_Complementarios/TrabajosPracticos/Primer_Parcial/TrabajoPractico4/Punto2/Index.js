const Mongoose = require('mongoose');
const Express =require('express');
const App = Express();
const {CADENA_CONEXION, PORT} = require('./config');
const {Noticias} = require('./Models');

App.use(Express.json());

const PeticionGet = async ()=> 
{
    await Mongoose.connect(CADENA_CONEXION, {useUnifiedTopology:true, useNewUrlParser:true}).then(()=>
    {
        console.log('Conexion exitosa');
    });
    await App.get('/Documentos', (req, res)=>
    {
        Noticias.find({}, (error, docs)=>
        {
            res.send(
                {
                    docs
                });
        });
    }
    );
} 
PeticionGet();

App.listen(PORT, ()=> {console.log(`App corriendo por el puerto ${PORT}, revisa en tu Navegador la 
dirección Localhost:${PORT}/Documentos`)});