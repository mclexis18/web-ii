const Mongoose = require('mongoose');
const {Schema} = Mongoose;

const NoticiasSchema = 
new Schema (
{

    Titulo:{ type:String },
    Enlace:{ type:String}

}
,
{timestamps:{createdAt:true, updatedAt:true}}
);
module.exports = Mongoose.model("Noticias", NoticiasSchema);