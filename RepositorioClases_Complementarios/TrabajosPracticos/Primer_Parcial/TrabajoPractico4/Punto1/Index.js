const Express = require('express');
const Fs = require('fs');
const Cors = require('cors');
/**Para acceder directamente a los valores de las variables de entorno
 * se debe destructurar el arrelo, me lleve 2 horas en ese error ajjajajja
 */
const {PORT} = require('./config');
const Home = Fs.readFileSync('./Public/Arquitectura.html');
const Server = Express();
/**llamamos la cabezera o pedido http */
Server.use(Cors()).use(Express.json());

Server.get('/' , (req, res)=>
{
    res.write(Home);
    res.end();
});

/**Ponemos a escuchar al servidor en nuestro puerto  */
/**dentro del cual nos saldra la pagina web */
Server.listen(PORT, ()=>
{
    console.log(`Server successfull in the Port: ${PORT} `);
});



