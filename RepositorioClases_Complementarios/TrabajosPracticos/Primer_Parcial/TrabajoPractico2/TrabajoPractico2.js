/**Trabajo practico 2 */
/**Aplicar promesas y async/await para solucionar un problema aplicado 
 * a su proyecto autónomo que tenga por lo menos 3 arreglos. 
Estos 3 arreglos deben tener relación con el proceso principal 
de su proyecto. Por ejemplo, si fuese consultar una reservación de hotel:
 Cabecera de reservación, detalle de cuartos por reservación y 
 detalle de personas que van a ubicarse por cada cuarto en la reservación
 */

 /**Oferta de cursos de diseño y animacion online  */

 /**inscripcion es.. temas...  */
const Cursos =
[
    {
        Id:1,
        Descripcion: "Edición de Videos",
        Duracion:"25 horas",
        Costo:"20.00 $",
        Instructor:
        {
            Nombre:"Santiago Alejandro Macías",
            Edad:"22 años",
            Profesion:"Lic. Diseño"
        },
        Temas: ["Instalacion de Filmora", "Marcas de Agua", "Tipos de Ediciones"]
       
    },
    {
        Id:2,
        Descripcion: "illustration en adobe illustrados",
        Duracion:"15 horas",
        Costo:"12.00 $",
        Instructor:
        {
            Nombre:"Santiago Alejandro Macías",
            Edad:"22 años",
            Profesion:"Lic. Diseño"
        },
        Temas: ["Instalacion ilustrados", "Modelos de ilustracion", "Plantillas en uso"]
       
    },
    {
        Id:3,
        Descripcion: "adobe phothosop ",
        Duracion:"18 horas",
        Costo:"13.00 $",
        Instructor:
        {
            Nombre:"Santiago Alejandro Macías",
            Edad:"22 años",
            Profesion:"Lic. Diseño"
        },
        Temas: ["Instalacion de adobe photoshop", "Desarrollo de capas", "Licencia de uso"]
    },
    {
        Id:4,
        Descripcion: "Diseño de publicidad",
        Duracion:"20 horas",
        Costo:"15.00 $",
        Instructor:
        {
            Nombre:"Santiago Alejandro Macías",
            Edad:"22 años",
            Profesion:"Lic. Diseño"
        },
        Temas: ["Analisis del Mercado", "La expansion de los mercados", "colores y texturas"]
    },
    {
        Id:5,
        Descripcion: "Creación de animaciones ",
        Duracion:"40 horas",
        Costo:"25.00 $",
        Instructor:
        {
            Nombre:"Santiago Alejandro Macías",
            Edad:"22 años",
            Profesion:"Lic. Diseño"
        },
        Temas: ["La historia de la Animacion", "Extraccion de animaciones", "Desarrollo de animaciones"]
    },
    {
        Id:6,
        Descripcion: "Dibujo Analogo y Digital",
        Duracion:"30 horas",
        Costo:"22.00 $",
        Instructor:
        {
            Nombre:"Santiago Alejandro Macías",
            Edad:"22 años",
            Profesion:"Lic. Diseño"
        },
        Temas: ["Vectores", "Lineas de Recorte", "Posicionamientos"]
    }
]

const Acreedor = 
[
    {
        Id:1,
        Nombres:"Alexis Ricardo",
        Apellidos:"López Mero",
        NCedula:1313637273,
    },
    {
        Id:2,
        Nombres:"Karen Stefania",
        Apellidos:"Santana Chavez",
        NCedula:1308336373,
        
    },
    {
        Id:3,
        Nombres:"Jesús Abel",
        Apellidos:"Gutierrez Vélez",
        NCedula:1303635363,
    },
    {
        Id:4,
        Nombres:"Dally Estefania",
        Apellidos:"Muñoz Velez",
        NCedula:1303633538
    }

]
const Inscripcion = 
[
    {
        Id:1,
        IdAcreedor:1,
        IdCurso:1

    },
    {
        Id:2,
        IdAcreedor:2,
        IdCurso:3
    },
    {
        Id:3,
        IdAcreedor:4,
        IdCurso:5
    }
]
const BuscandoInscripcion = (IdInscripcion)=> new Promise ((resuelve, rechaza)=>
{
    const inscripcion = Inscripcion.find((Encontrar)=> Encontrar.Id === IdInscripcion);
    resuelve(inscripcion);
   if (!inscripcion)
   {
        const NUEVOERROR = new Error();
        rechaza(console.log(NUEVOERROR.message ="Inscripcion no encontrada"));
        
   } 
});

 const BuscandoAcreedor = (IdAcreedor) => new Promise((resuelve, rechaza)=>
 {
    const acreedor = Acreedor.find(Encontrar=> Encontrar.Id === IdAcreedor);
    resuelve(acreedor);
    if (!inscripcion)
   {
        const NUEVOERROR = new Error();
        rechaza(console.log(NUEVOERROR.message ="Acreedor no encontrada"));
          
   } 
 });

 const BuscandoCurso =(IdCurso) => new Promise((resuelve, rechaza)=> 
 {
    const curso = Cursos.find(Encontrar => Encontrar.Id === IdCurso);
    resuelve(curso);
    if (!curso)
   {
        const NUEVOERROR = new Error();
        rechaza( console.log(NUEVOERROR.message ="curso no encontrado") )
       
   } 
 });

 const LlamandoFunciones = async () => 
 {
     try 
     {
        const INSCRIPCION = await BuscandoInscripcion(3);
        const ACREEDOR = await BuscandoAcreedor(INSCRIPCION.IdAcreedor);
        const CURSOS = await BuscandoCurso(INSCRIPCION.IdCurso);
        console.log(`Felicidades ${ACREEDOR.Nombres} su curso inscrito es ${CURSOS.Descripcion} y los temas que usted veran son ${CURSOS.Temas}`);
         
     } catch (error) {
        console.log(error.message);     
     }
    
 }

 LlamandoFunciones();