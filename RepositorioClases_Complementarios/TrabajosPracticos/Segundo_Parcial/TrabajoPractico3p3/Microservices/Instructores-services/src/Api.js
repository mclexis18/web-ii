const Express = require('express');
const App = Express();

const Datos = {

    data:[],
    Services:[],
    Arquitectura:'MicroServicios'

}

App.use((req, res, next)=>{

    Datos.data=[];
    Datos.Services=[];
    next();

});

App.get('/Api/V2/Instructores', (req, res)=>{

    Datos.data.push('AlejandroV2', 'SantiagoV2', 'RoberthV2');
    Datos.Services.push('Servicio de Instructores');
    res.send(Datos);

});

module.exports= App;