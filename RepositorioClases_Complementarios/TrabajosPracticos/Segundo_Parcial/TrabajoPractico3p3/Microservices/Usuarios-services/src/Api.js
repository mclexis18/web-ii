const Express = require('express');
const App = Express();

const Datos = {

    data:[],
    Services:[],
    Arquitectura:'MicroServicios'

}

App.use((req, res, next)=>{

    Datos.data=[];
    Datos.Services=[];
    next();

});

App.get('/Api/V2/Usuarios', (req, res)=>{

    Datos.data.push('AlexisV2', 'NardineV2', 'KarenV2');
    Datos.Services.push('Servicio de Usuarios');
    res.send(Datos);

});

module.exports= App;