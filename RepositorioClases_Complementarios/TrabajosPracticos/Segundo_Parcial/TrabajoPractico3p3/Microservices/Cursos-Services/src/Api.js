const Express = require('express');
const App = Express();

const Datos = {

    data:[],
    Services:[],
    Arquitectura:'MicroServicios'

}

App.use((req, res, next)=>{

    Datos.data=[];
    Datos.Services=[];
    next();

});

App.get('/Api/V2/Cursos', (req, res)=>{

    Datos.data.push('Diseño GraficoV2', 'After effectV2', 'Canvas y trazosV2');
    Datos.Services.push('Servicio de Cursos');
    res.send(Datos);

});

module.exports= App;