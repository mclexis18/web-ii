const Express =require('express');
const App = Express();

const Datos = {
    data:[],
    Servicios:[],
    Arquitectura:'Monolitica'
}

App.use((req,res, next)=>{

    Datos.data=[];
    Datos.Servicios=[];
    next();

});


App.get('/Api/V1/Cursos', (req, res)=>{

    Datos.data.push('Diseño Grafico', 'After effect', 'Canvas y trazos');
    Datos.Servicios.push('Servicio de Cursos');
    res.send(Datos);

});

App.get('/Api/V1/Usuarios', (req, res)=>{

    Datos.data.push('Alexis', 'Nardine', 'Karen');
    Datos.Servicios.push('Servicio de Usuarios');
    res.send(Datos);

});

App.get('/Api/V1/Instructores', (req, res)=>{

    Datos.data.push('Alejando', 'Santiago', 'Robertho');
    Datos.Servicios.push('Servicio de Instructores');
    res.send(Datos);

});

module.exports = App;