const Container = require('./src/Startup/Container');

/**con el .resolve vamos a tener todo lo que guardamos en el contenedor  */
const Server = Container.resolve('App');

/**Cremoa suna instancia de la app del contenedor  */
const {CONEXION_MONGO} = Container.resolve('Config');

const Mongoose = require('mongoose'); 

/**Parametro de la base de datos para crear los Index */
Mongoose.set("useCreateIndex", true);
Mongoose.connect(CONEXION_MONGO, {useNewUrlParser:true, useUnifiedTopology:true})
.then(()=> Server.Start()) 
.catch(console.log);
