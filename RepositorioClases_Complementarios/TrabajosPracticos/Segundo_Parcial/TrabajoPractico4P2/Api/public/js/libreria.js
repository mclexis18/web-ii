window.addEventListener('load', function () {

    let HtmlGenerado="";
    HtmlGenerado+= `
    <label for="txtid">ID</label>
    <input type="text" id="txtid">
    <label for="txtname">Nombre</label>
    <input type="text" id="txtname">
    <label for="txtusername">Nombre del Usuario</label>
    <input type="text" id="txtusername">
    <label for="txtpassword">Contraseña</label>
    <input type="text" id="txtpassword">
    <button id="btnnuevo"> Nuevo</button>
    <button id="btngrabar">Grabar</button>
    <button id="btnmodificar">Modificar</button>
    <button id="btnconsultar">Consultar</button>
    <button id="btneliminar"> Eliminar</button>
    <label for="tipousuario">Tipo De Usuario</label>
    <select id="tipousuario"></select>
    <div id="divcontenido"></div>`;

    htmlCuerpo.innerHTML = HtmlGenerado;

    /**Escogiendo tipo de usuario */
    
    /**Realizamos la llamada de tipo de usuario mediante el async await */

    (async()=>{

        /**Esperamos a que carguen los datos */
        const Datos = await fetch(`http://localhost:5000/api/v1/TipoUsuario`);

        /**Convertimos esos datos en json y los esperamos(await) porque tambien devuelve una promesa */
        const DatosEstricturados = await Datos.json();

        /**Agregamos una opcion Inicial que no muestre nada */
        tipousuario.innerHTML+=`<option selected disabled hidden style='display: none' value=''></option>`;

        /**Agregamos los tipos de usuarios al select */
        DatosEstricturados.forEach(TipoUsuarios=>{

            /**En caso de no existir TiposDeUsuarios undefined se muestra un msj por consola */
            /**No es muy necesario para el funcionamiento, pero puede servir de referencia */
            if (TipoUsuarios.tipousuario === undefined) {
                
                console.log('No hay Usuarios Definidos');

            }else
            /**Agregamos los tipos de usuario al select con los datos obtenidos */
            tipousuario.innerHTML+=`<option>${TipoUsuarios.tipousuario}</option>`
        });

    })()
        
    btnnuevo.addEventListener('click', function (){

        
        txtid.value='';
        txtname.value='';
        txtusername.value='';
        txtpassword.value = '';

    });

    btngrabar.addEventListener('click', function () {
        
        let URL = `http://localhost:5000/api/v1/user`;
        let data = {

            name: txtname.value,
            username: txtusername.value,
            password: txtpassword.value
        }

        fetch(URL, {

            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type':'application/json'
            }

        }).then(res=> res.json())
        .then(res2=> console.log(res2))
        .catch(error=> console.error('error', error))
        
    })

    btnmodificar.addEventListener('click', function(){
        
        let URL = `http://localhost:5000/api/v1/user/${txtid.value}`;
        let data = {

            name: txtname.value,
            username: txtusername.value,
            password: txtpassword.value
        }

        fetch(URL, {
            method: 'PATCH',
            body: JSON.stringify(data),
            headers:{
                'Content-Type':'application/json'
            }

        }).then(res=> res.json())
        .catch(error=> console.error('error', error));

    })

    btnconsultar.addEventListener('click', function () {
        fetch(`http://localhost:5000/api/v1/user`).then(resultado=>{
        return resultado.json()
    }).then(consulta=>{
        console.log(consulta);
        let tabla= "<table border=1>"
        for(const indiceElemento in consulta)
        {
            tabla+="<tr>";
            const actual =  consulta[indiceElemento];
            tabla+=`<td>${actual.name}</td>`
            tabla+=`<td>${actual.password}</td>`
            tabla+=`<td> <button value='${actual._id}'>${actual.username}</button> </td>`
            tabla+="</tr>"
        }
        tabla+="</table>"
        divcontenido.innerHTML= tabla;

    })

    })

    btneliminar.addEventListener('click', function () {

        let URL = `http://localhost:5000/api/v1/user/${txtid.value}`;

        fetch(URL, {

            method:'DELETE'

        }).then(res=> res.json())
        .then(res2=> console.log(res2))
        .catch(error=> console.error('error', error));

    })
    
})
