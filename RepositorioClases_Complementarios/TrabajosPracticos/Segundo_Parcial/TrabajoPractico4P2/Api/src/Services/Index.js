/**Este es como el modulo 
 * de exportacion de los modelos 
 */
module.exports = 
{
    /**Asi podems exportar los demas services
     * como si fuera parte de una rreglo
     * es por ello que se dice que todo en javascript es un arreglo 
     */
    HomeService: require('./Home.Service'),
    UserServices:require('./User.Services'),
    TipoUserService:require('./TipoUserServices')

}