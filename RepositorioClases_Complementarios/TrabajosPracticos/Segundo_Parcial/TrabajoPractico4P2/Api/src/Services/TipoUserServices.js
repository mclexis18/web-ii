const BaseService = require('./Base.Services');

let _TipoUserRepository = null;

class TipoUserService extends BaseService{

    constructor({TipoUserRepository}){

        super(TipoUserRepository);
        _TipoUserRepository = TipoUserRepository;

    }

}

module.exports = TipoUserService;