const {Router} = require('express');

module.exports = function ({TipoUserController}) {
 
    const router = Router();

    router.get('/:TipoUserId', TipoUserController.get);
    router.get('', TipoUserController.getAll);
    router.post('', TipoUserController.create);
    router.patch('/:TipoUserId', TipoUserController.update);
    router.delete('/:TipoUserId', TipoUserController.delete);

    return router;
    
}