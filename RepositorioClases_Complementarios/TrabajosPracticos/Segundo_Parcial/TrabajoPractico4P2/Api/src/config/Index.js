if ( process.env.NODE_ENV != "production" )
{
    require('dotenv').config();
}
module.exports = 
{
    PORT:process.env.PORT,
    CONEXION_MONGO:process.env.CONEXION_MONGO,
    APPLICATION_NAME:process.env.APPLICATION_NAME
}
