/**Tiene un controlador porque se va a ingresar inyeccion de dependencia */
let _HomeService = null;
class HomeController{
    /**Lo que hicimos en el service lo podemos acceder con inyeccion de dependencia */
    /**Para la Inyeccion de dependencia debe ser el mismo nombre de el index donde se exporta 
    */
    constructor({HomeService})
    {
        /**Con esto ya esta disponible el metodo index por medio de _HomeService */
        /**Controlador es el handdler de las rutas */
        _HomeService= HomeService;
    }
    Index(req, res)
    {
        /**Devolvemos el metodo iNdex que esta en la clase de Services */
       return res.send(_HomeService.Index());  
    }
    async Saludos(req, res){
        return res.send('Hola mundo ');
    }
}

/**Exportamos la clase de HomeController */
module.exports= HomeController;