let _TipoUserService = null;

class TipoUserController{

    constructor({TipoUserService}){

        _TipoUserService= TipoUserService;
      
    }

    //get
    async get(req, res){

        const {TipoUserId} = req.params;
        const TipoUser = await _TipoUserService.get(TipoUserId);
        return res.send(TipoUser);

    } 
    //getAll

    async getAll(req, res){
        const TipoUsers = await _TipoUserService.getAll();
        return res.send(TipoUsers);
    }

    //create
    async create(req, res){

        const {body} =req; 
        const CreateTipoUser = await _TipoUserService.create(body);
        return res.send(CreateTipoUser);

    }

    //update
    async update(req, res){

        const {TipoUserId} = req.params;
        const {body} = req;
        const UpdateTipoUser = await _TipoUserService.update(TipoUserId, body);
        return res.send(UpdateTipoUser);

    }

    //delete
    async delete(req, res){

        const {TipoUserId} =req.params;
        const DeleteTipoUser = await _TipoUserService.delete(TipoUserId);
        return res.send(DeleteTipoUser);

    }

}

module.exports = TipoUserController;