const {Schema, model} =require('mongoose');

const {genSaltSync, compareSync, hashSync, getSalt} =require('bcryptjs');

const UserSchema = new Schema({

    name:{type:String, required:true},
    username:{type:String, required:true},
    password:{type:String, required:true},

});

UserSchema.methods.toJSON = function(){
    let User =  this.toObject();
    delete password;
    return User;
}
UserSchema.methods.ComparePassword = function(password){
  return compareSync(password, this.password);
}
/**Antes de insertar un dato  */
UserSchema.pre('save', async function(next){


    const User = this;
    if(User.isModified("password")){
        
        next();

    }
    const Salt = genSaltSync(10);
    const HasPassword = hashSync(User.password, Salt);
    User.password = HasPassword;
    next();

});


module.exports = model('User', UserSchema);