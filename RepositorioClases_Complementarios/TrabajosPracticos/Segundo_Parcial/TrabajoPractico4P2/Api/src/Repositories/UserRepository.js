const BaseRepository = require('./BaseRepository');
let _user = null;
class UserRepository extends BaseRepository{

    constructor({User}){ 
        
        /**Aqui pasamos el modelo a la clase padre en la cual
         * agarramos el user
         */
        super(User);
        _user = User;
    }

    /**Podemos agregar funcionalidadess Propias en las demas clases 
     * diferentes del repositorio
     */
    async getUserByUserName(username){
        return await _user.findOne(username);
    }

}

module.exports = UserRepository;