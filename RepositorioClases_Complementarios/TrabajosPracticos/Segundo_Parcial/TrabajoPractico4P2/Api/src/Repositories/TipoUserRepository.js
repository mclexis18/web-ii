const BaseRepository = require('./BaseRepository');
let _tipouser = null;

class TipoUserRepository extends BaseRepository{

    constructor({TipoUser}){

        super(TipoUser);
        _tipouser = TipoUser;

    }

}

module.exports = TipoUserRepository;