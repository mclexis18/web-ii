const InforEmpresa = async (msg)=> new Promise((resolve, reject)=>
{
    if (msg.body === '2')
    {
        resolve(msg.reply( 
            "📍 *Ubicación:* Calle 122, Av. 124 \n"+
            " \n"+
            "🕐 *Horarios de atención:* De lunes a viernes, de 08:00 a 17:00  \n"+
            " \n"+
            "🖱️ *Página web:* https://cursos-dise.web.app \n"+
            " \n"+
            "↩️ Si desea volver al menú de opciones digite '*' "
            )); 
  
    }
});

module.exports = InforEmpresa;