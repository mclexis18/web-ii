const CodigoQr = require('qrcode-terminal');
const Cliente = require('whatsapp-web.js');
const {APPLICATION_NAME} =require('../config/Index');



/**Instanciamos el cliente de WS */
const cliente = new Cliente.Client();

/**Pasamos el codigo QR */
cliente.on('qr', (Codigo)=>
{
    CodigoQr.generate(Codigo, {small:true});
});


/**Esperemos a que la aplicacion este lista */
cliente.on('ready', ()=>
{
    console.log(`El chatBot de ${APPLICATION_NAME} esta Funcionando`);
});

cliente.initialize();


/**Exportamos el cliente de whatsapp  */

module.exports = cliente;