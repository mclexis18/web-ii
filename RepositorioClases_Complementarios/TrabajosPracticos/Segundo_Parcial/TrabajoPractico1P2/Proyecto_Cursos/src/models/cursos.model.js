const mongoose = require("mongoose");
const { Schema } = mongoose;

const CursoSchema =   
new Schema(
{
    id_curso: { type:String },
    name_materia: { type:String },
    duracion: { type:String },
    costo: { type:String }, 
    //instructor: {},
    temas: {type:String}   
}
);

/**Exportamos los modelos  */
module.exports  =  mongoose.model("Curso", CursoSchema );





//CommentSchema.plugin(require("mongoose-autopopulate"));

/*
Descripcion: "Dibujo Analogo y Digital",
Duracion:"30 horas",
Costo:"22.00 $",
Instructor:
{
    Nombre:"Santiago Alejandro Macías",
    Edad:"22 años",
    Profesion:"Lic. Diseño"
},
Temas: ["Vectores", "Lineas de Recorte", "Posicionamientos"]
}
]*/
