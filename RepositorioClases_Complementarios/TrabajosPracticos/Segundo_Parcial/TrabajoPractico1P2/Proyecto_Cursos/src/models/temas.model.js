const mongoose = require("mongoose");
const { Schema } = mongoose;

const TemaSchema =   
new Schema(
            { descripcion: { type:String, require:true }}
    );

module.exports  =  mongoose.model("Tema", TemaSchema );