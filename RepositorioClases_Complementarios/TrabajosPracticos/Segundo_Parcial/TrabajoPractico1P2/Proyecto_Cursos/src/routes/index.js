const {CursoRoutes} = require('./index.routes');
const Express = require('express');
const Api = Express();

Api.use(Express.json());
Api.use('/Home', CursoRoutes);

module.exports = Api;