const { Router } = require('express');
//const { check } = require('express-validator');

const { UsuarioController } = require('../controllers');

const router = Router();

//recuperar todos los usuarios registrados en la BD
router.get('/', UsuarioController.findAll);

//recuperar 1 usuario de la BD con los nombres del usuario
router.get('/:nomApe', UsuarioController.findOne);

// crear un usuario interesado en los cursos
router.post('/', UsuarioController.create);

// modificar datos de la BD con los nombres del usuario
router.put('/:nomApe', UsuarioController.update);

// eliminar un usuario de la BD con los nombres del usuario
router.delete('/:nomApe', UsuarioController.delete);

/** exportar rutas */
module.exports = router;
