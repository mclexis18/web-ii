//Se llaman a las depencias a utilizar, y se exporta el modelo "curso" que se encuentra en la carpeta modelo
//const Express = require('express'); 
const {Curso} =require('../models');
//const App = Express();

//Se llama a la app junto con la dependencia express.json
//App.use(Express.json());



/**Create- POST */
//Se crea una función con el nombre create que es la que creará un nuevo curso
// Ruta: /Cursos
   const create = async (req, res)=>{
            const nuevoCurso= await new Curso(req.body);
            nuevoCurso.save()
            
            res.json(nuevoCurso)
    }



/**Read - GET*/
// Ruta:/Cursos
//Se crea una función con el nombre traer que es la que recogerá los datos de los cursos y los mostrará
const  traer = async (req, res) =>{ 
   
        const CursosGuardados = await Curso.find({});

        res.json(CursosGuardados);
    
};


/**Read id- GET por id*/
//Se crea una función con el nombre traerId que es la que recogerá los datos de uno de los cursos por medio de id
// Ruta:/Cursos/:id
const TraerId = async(req, res)=>
{
    
        const CursosGuardados = await Curso.findById(req.params.id);
        //Si el id no está en la base de datos, se muestra el mensaje
        //if(!CursosGuardados) res.status(404).send("No existe curso con ese id");
        //Si el id si está en la base de datos, se muestran los datos
        //if(CursosGuardados) 
        res.json(CursosGuardados);
        
   
}


/**update -PUT */
//Se crea una función con el nombre update(actualizar) que es la que actualizará los datos 
//del curso 
// Ruta:/Cursos/:id
const update = async (req, res)=>{
    
        
        const modificar= await Curso.findByIdAndUpdate(req.params.id, req.body, {useFindAndModify: false})
        await modificar.save()
        //res.send('Curso Modificado')
        res.json({
            msg: 'Curso Modificado'
        });
    
    
}

/**Delete*/
//Se crea una función con el nombre deleted que es la que eliminará los datos de los cursos
// Ruta:/Cursos/:id

const deleted = async (req, res)=>{
    

        const EliminaCurso = await Curso.findByIdAndDelete(req.params.id);
        /*res.send('Curso eliminado');*/
        res.json({
            msg: 'Curso Eliminado'
        });

       
     
}



//const NuevoCurso = Cursos({Nombre:"Diseño", Costo:

//Se exportan todas las funciones
module.exports={
    create,
    traer,
    TraerId,
    update,
    deleted
}