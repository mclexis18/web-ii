const {connect} = require('mongoose');
const {CADENA_CONEXION, PORT} = require('./src/config');
const Server = require('./src/Startup');


(async()=>
{
    await connect(CADENA_CONEXION, {useNewUrlParser:true, useUnifiedTopology:true}).then(()=>
    {
        console.log('Conexion exitosa con la base de datos');
    }).catch(()=> console.log('No se pudo conectar'));
    await Server.LevantarPuerto();
})();





