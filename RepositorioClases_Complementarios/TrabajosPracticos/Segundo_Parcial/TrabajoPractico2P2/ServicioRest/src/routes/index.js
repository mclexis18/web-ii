const Express = require('express');

module.exports = function ({CursoRoutes, UsuarioRoutes}) {

    const Route = Express.Router();
    const ApiRoutes = Express.Router();

    ApiRoutes
    .use(Express.json());

    ApiRoutes.use('/Cursos', CursoRoutes);
    ApiRoutes.use('/Usuarios', UsuarioRoutes)
    ApiRoutes.use('/Hola', (req, res)=>{
        res.send('Hola Mundo');
    })

    Route.use('/api/v1', ApiRoutes);

    return Route;

    /**Acceder por */
    /**localhost:puerto/api/vi/Ruta */
    
}