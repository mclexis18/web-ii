const { Router } = require('express');
const { check } = require('express-validator');

const Express = require('express');
const Api = Express();

const { CursoController} = require('../controllers/index');
const {validarCampos}= require('../middlewares/validarCampos');
const {existeCursoPorId}= require('../middlewares/validarDB');

  const router = Router();
  Api.use(Express.json());

//recuperar todos los cursos registrados en la BD
router.get('/Cursos', CursoController.traer);



//recuperar 1 curso de la BD con el id del curso
router.get('/Cursos/:id',[
  check('id', 'No es un ID válido').isMongoId(),  //VALIDA QUE EL ID SEA DEL TIPO ID_MONGOOSE
  check('id').custom(existeCursoPorId),           //VALIDA SI EL CURSO EXISTE O NO EN LA BD
  validarCampos
], CursoController.TraerId);



// crear un curso en la BD 
router.post('/Cursos', CursoController.create);



// modificar datos de la BD con el id del curso
router.put('/Cursos/:id',[
  check('id', 'No es un ID válido').isMongoId(),
  check('id').custom(existeCursoPorId),
  validarCampos
], CursoController.update);



// eliminar 1 curso de la BD con el id del curso
router.delete('/Cursos/:id',[
  check('id', 'No es un ID válido').isMongoId(),
  check('id').custom(existeCursoPorId),
  validarCampos
], CursoController.deleted);



/** exportar rutas */
module.exports = router;