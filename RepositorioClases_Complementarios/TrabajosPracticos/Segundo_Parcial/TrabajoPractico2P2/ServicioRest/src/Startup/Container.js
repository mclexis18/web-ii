const {createContainer, asValue, asClass, asFunction} = require('awilix');

/**Variables de entorno */

const Config = require('../config/index');

/**Modelos mongoose */

const {Curso, Usuario} = require('../models/index');

/**Repositorios */

const {CursosRepository, UsuarioRepository} = require('../Repositories/Index');

/**Services */

const {CursosServices, UsuariosServices} = require('../Services/Index');

/**Controllers */

const {CursoController, UsuarioController} =require('../controllers/index');

/**Rutas */

const {CursoRoutes,UsuarioRoutes} =require('../routes/index.routes');

/**Rutas Index */

const Routes = require('../routes/index');

/**App puerto */

const App = require('./index');


const Contenedor = createContainer();

Contenedor
    /**registro variables y mas*/
    .register
    ({
        Config:asValue(Config),
        Routes:asFunction(Routes).singleton(),
        App:asClass(App).singleton()
    })
    /**Registro Modelos */
    .register
    ({
        Curso:asValue(Curso),
        Usuario:asValue(Usuario)
    })
    /**Registro de repositios */
    .register
    ({
        CursosRepository:asClass(CursosRepository).singleton(),
        UsuarioRepository:asClass(UsuarioRepository).singleton()  
    })
    /**Registro de services */
    .register
    ({
        CursosServices:asClass(CursosServices).singleton(),
        UsuariosServices:asClass(UsuariosServices).singleton()
    })
    /**Registro de controller */
    .register
    ({
        CursoController:asClass(CursoController).singleton(),
        UsuarioController:asClass(UsuarioController).singleton()
    })
    /**Registro de Rutas */
    .register
    ({
        CursoRoutes:asFunction(CursoRoutes).singleton(),
        UsuarioRoutes:asFunction(UsuarioRoutes).singleton()
    })

module.exports = Contenedor;