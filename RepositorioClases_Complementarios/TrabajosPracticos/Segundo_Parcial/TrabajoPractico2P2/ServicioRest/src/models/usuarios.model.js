const mongoose = require("mongoose");
const { Schema } = mongoose;

const UsuarioSchema =   
new Schema(
{
    //identificacion: { type:String, require:true, max:10 },
    idUsuario: {type:String, require:true},
    nombre: { type:String, require:true },
    numero: {type:String, require:true},
      
},
{
     timestamps:{ craetedAt: true } 
}
);

module.exports  =  mongoose.model("Usuario", UsuarioSchema );
