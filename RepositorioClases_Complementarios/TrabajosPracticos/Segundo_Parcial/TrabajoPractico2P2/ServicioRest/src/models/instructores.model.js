const mongoose = require("mongoose");
const { Schema } = mongoose;

const InstructorSchema =   
new Schema(
{
    identificacion: { type:String, require:true, max:10 },
    nombres: { type:String, require:true },
    f_nacimiento: { type:Date },
    direccion: { type:String },
    telefono: { type:String, require:true, max:10 },
    profesion: { type:String }
      
},
{
     timestamps:{ craetedAt: true, updatedAt:true } 
}
);

module.exports  =  mongoose.model("Instructor", InstructorSchema );
