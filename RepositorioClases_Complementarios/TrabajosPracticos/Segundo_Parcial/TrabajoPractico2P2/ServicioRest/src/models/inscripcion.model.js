const mongoose = require("mongoose");
const { Schema } = mongoose;

const InscripcionSchema =   
new Schema(
{
    curso: {
        type: Schema.Types.ObjectId,
        ref: "cursos" ,
        required: true,
        autopopulate: true
      },
    alumnos: [{ 
        type: Schema.Types.ObjectId, 
        ref: 'alumnos',
        required: true,
        autopopulate: true 
    }]
      
},
{
     timestamps:{ craetedAt: true, updatedAt:true } 
}
);

/**Para relacionar las tablas el mongoose-autopopulate */
CommentSchema.plugin(require("mongoose-autopopulate"));
/**Exportamos los modelos  */
module.exports  =  mongoose.model("Inscripcion", InscripcionSchema );