const Libros = [
    {
        Id:1,
        Titulo:"JavaScript para el cliente",
        idautor:3

    },
    {
        Id:2,
        Titulo:"JavaScript para el servidor",
        idautor:1
    },
    {
        Id:3,
        Titulo:"Funciones base de datos",
        idautor:2
    }
];
const Autor = [
    {
        Id:1,
        Nombre:'Andes Guerrero'
    },
    {
        Id:2,
        Nombre:'Nicolas Cevallos'
    },
    {
        Id:3,
        Nombre:'Nardine Ajala'
    }
];

async function BuscandoLibro(IdEncontrar) 
{
   
        const Libro = Libros.find((Libro1)=> Libro1.Id === IdEncontrar);
        /**Verificamos si no existe */
        if (!Libro) 
        {
            /**Creamos nuestros errores */
            const NuevoError = Error();
            NuevoError.message="Sigue buscando no encontramos el libro";
            /**Reject es igual a rechazo */
            throw NuevoError;
        }
        return Libro;
        /**Si no hay rechazo, devolvemos la libro */
        
    
}
/**Es la misma logica que el ejercicio anterior */
function BuscandoAutor(IdEncontrar) 
{
   
        const autor = Autor.find((autor1)=> autor1.Id === IdEncontrar);
        if (!autor) 
        {
            const NuevoError = Error();
            NuevoError.message="Sigue buscando no encontramos el autor"
           throw NuevoError;   
        }
        return autor;
        
}

async function  LlamandoAsync()
{
    try 
    {
        const Libroasync = await BuscandoLibro(10);
        const Autorasync = await BuscandoAutor(Libroasync.idautor);
        console.log(`Felicidades su libro es: ${Libroasync.Titulo} y su autor es: ${Autorasync.Nombre} `);
        

    } catch (error) {
        console.log(error.message);
    }    
}
LlamandoAsync();