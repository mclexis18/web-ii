/**Practica vienes 21/05/2021 */

const Libros = [
    {
        Id:1,
        Titulo:"JavaScript para el cliente",
        idautor:3

    },
    {
        Id:2,
        Titulo:"JavaScript para el servidor",
        idautor:1
    },
    {
        Id:3,
        Titulo:"Funciones base de datos",
        idautor:2
    }
]
const Autor = [
    {
        Id:1,
        Nombre:'Andes Guerrero'
    },
    {
        Id:2,
        Nombre:'Nicolas Cevallos'
    },
    {
        Id:3,
        Nombre:'Nardine Ajala'
    }
]
function BuscarLibro(Id, callbackLibro) 
{
    const libro = Libros.find((Encuentralo)=> Encuentralo.Id === Id);
    if (!libro) 
    {
        /**Tener mucho cuidado con las variables,
         * si poniamos error con mayuscula iba a tomar como si la variable
         * misma se estuviera llamando 
         */
        const error = new Error();
        error.message = "El libro no se pudo encontrar";
        /**Si no colocamos return se devolvera 2 veces */
        return callbackLibro(error);
    }
    callbackLibro(null, libro);
/**primero devolvemo el error y despues devolvemos sin error */
}
/**Quiero recalcar que para mi entendimiento el callback es el concepto, pero en si es una funcion pasada dentro de otra 
 * y sus parametros tu los puedes definir 
 */
function BuscarAutorPorId(Id, CallbackAutor)
{
    /**Mucho cuidado con confundir las variables, al ser nombradas pueden coinidir con otras y pueden generar errores :v */
    const autor = Autor.find((EncuentraAutor)=> EncuentraAutor.Id===Id );
    if (!autor) 
    {
        const error = new Error();
        error.message ="Autor sin encontrar continua participando";
        return CallbackAutor(error);     
    }
    return CallbackAutor(null, autor);
}
BuscarLibro(3, (Error, Libro)=>
{
    if(Error)
    {
        return console.log(Error.message);
    }
    BuscarAutorPorId(Libro.idautor, (ErrorLibro, Autor)=>
    {
        if (ErrorLibro) {
            return console.log(ErrorLibro.message);
        }
        /**En el libro con ese id le agregamos 
         * el autor relazionado 
         */
        Libro.Autor = Autor;
        /**Evitamos el Id para que no sea redundante */
        delete Libro.idautor;
        /**Esta vez solo mostraremos lo que queramos  */
        console.log(`El libro que eligio es ${Libro.Id} Su nombre es el libro ${Libro.Titulo} y su autor es ${Libro.Autor.Nombre}`);
    });
});