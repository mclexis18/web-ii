/**Haremos el mismo ejemplo de los libros, solo que esta vez con promesas */
const Libros = [
    {
        Id:1,
        Titulo:"JavaScript para el cliente",
        idautor:3

    },
    {
        Id:2,
        Titulo:"JavaScript para el servidor",
        idautor:1
    },
    {
        Id:3,
        Titulo:"Funciones base de datos",
        idautor:2
    }
]
const Autor = [
    {
        Id:1,
        Nombre:'Andes Guerrero'
    },
    {
        Id:2,
        Nombre:'Nicolas Cevallos'
    },
    {
        Id:3,
        Nombre:'Nardine Ajala'
    }
]

/**Creamos la función BuscandoLibro con un parametro Id, pero le colocamos Id a encontrar
 * la misma funcion retorna una promesa y todo la logica será dentro de la promesa que retirnaremos 
 */
function BuscandoLibro(IdEncontrar) 
{
    /**Parametros de las promesas */
    return new Promise((resolve, reject)=>{
        /**Buscamos el libro y lo comparamos con el Id a encontrar */
        /**Recordemos que este libro que esta justo aqui !*/
        /**cuenra con todos los atributos del libro como ! tal ya  */
        const Libro = Libros.find((Libro1)=> Libro1.Id === IdEncontrar);
        /**Verificamos si no existe */
        if (!Libro) 
        {
            /**Creamos nuestros errores */
            const NuevoError = Error();
            NuevoError.message="Sigue buscando no encontramos el libro";
            /**Reject es igual a rechazo */
            reject(NuevoError);    
        }
        /**Si no hay rechazo, devolvemos la libro */
        resolve(Libro);
    })
}
/**Es la misma logica que el ejercicio anterior */
function BuscandoAutor(IdEncontrar) 
{
    return new Promise((resolve, reject)=>{
        const autor = Autor.find((autor1)=> autor1.Id === IdEncontrar);
        if (!autor) 
        {
            const NuevoError = Error();
            NuevoError.message="Sigue buscando no encontramos el autor"
            reject(NuevoError);    
        }
        resolve(autor);
    })
}
/**En esta variable auxiliar guardamos el libro que encontramos para agregarle el autor  */
let GuardandoElLibro = {};
/**Pasamos el Id  */
BuscandoLibro(1).
then((Libro)=>
{
    /**Aqui guardamos el libro en el contenedor */
    GuardandoElLibro = Libro;
    /**Aqui con el libro ya encontramos en base a ese libro buscaremos el autor llamando 
     * a la otra funcion que retorna la promesa de los autores
     */
    return BuscandoAutor(GuardandoElLibro.idautor);
})
   /**como las promesas se cumplen con then, bueno pasamos el .then */
.then((Autor)=>{
    /**Agregamos el autor a ese contenedor donde guardamos el libro  */
    GuardandoElLibro.autor = Autor;
    /**Mostramos ese libro y sus parametros */
    console.log(GuardandoElLibro);
    /**Aunque podemos personalizarlo */
    delete GuardandoElLibro.idautor;
    console.log(GuardandoElLibro);
    /**Tambien podemos perzonalizarlo */
    console.log(`Nuestro titulo es: ${GuardandoElLibro.Titulo}, su autor es: ${GuardandoElLibro.autor.Nombre}`);
})
.catch((Errores)=> 
{
    console.log(Errores.message)
});