const Express = require('express');
const App = Express();

/**Datos que simularan la base de datos*/
const response = {
    
    data:[],
    services: "All services",
    architecture: "Monolothic"

}

/**Limpiamos la data */
App.use((req,res, next)=>{

    response.data=[];
    next(); 

});

/**Devuelve libros */
App.get("/api/v1/books", (req, res)=>{

    response.data.push('JavaScript version 1', 'Css conceptos basicos', 'Docker para estudiantes');
    return res.send(response);

});

/**Devuelve vehiculos */
App.get("/api/v1/cars" , (req, res)=>{

    response.data.push('Toyota 1100', 'Hyundai 2000', 'Volvo');
    return res.send(response);

});


/**devuelve Usuarios*/
App.get("/api/v1/users", (req, res)=>{

    response.data.push('Admin', 'Free', 'Premium');
    return res.send(response);

});

module.exports = App;