const Express = require('express');
const App = Express();

const response = {

    data:[],
    services:'Car Services',
    architecture:"Micro services"

}

const logger =message=> console.log(`servicio de ${message}`);

App.use((req, res, next)=>{

    response.data=[];
    next();

})

App.get('/api/v2/cars', (req, res)=>{

    response.data.push('Toyota', 'Hyundai', 'WV');
    logger('Get car');
    res.send(response);

});

module.exports = App;