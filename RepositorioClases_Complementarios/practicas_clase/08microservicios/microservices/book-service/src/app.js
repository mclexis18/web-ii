const Express = require('express');
const App = Express();

const response = {

    data:[],
    services:"Book services",
    architecture:"MicroServicios"

}

const logger =message=> console.log(`Book Services ${message}`);

App.use((req, res, next)=>{

    response.data= [];
    next();

})

App.get('/api/v2/books', (req, res)=>{

    response.data.push('Integracion microservicios', 'docker', 'SD');
    logger('get book data');
    return res.send(response);

});

/** Minuto 1:09:00*/

module.exports= App;