const Express = require('express');
const App = Express();

const response = {

    data:[],
    services:'User Services',
    architecture:"Micro services"

}

const logger =message=> console.log(`servicio de ${message}`);

App.use((req, res, next)=>{

    response.data=[];
    next();

})

App.get('/api/v2/users', (req, res)=>{

    response.data.push('Admin1', 'Invitado', 'Super');
    logger('Get Users');
    res.send(response);

});

module.exports = App;