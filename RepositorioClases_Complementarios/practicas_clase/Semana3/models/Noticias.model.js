const Mongoose = require("mongoose");
const { Schema } = Mongoose;

const NoticiaSchema =
/**No debemos olvidar definir el esquema o schema  */
 new Schema(
{
    /**Datos del esquema */
    Titulo:{ type:String },
    Enlace:{ type:String}
  
}
,
/**Marca de tiempo */
{timestamps: {createAt:true, updatedAt:true}}
);

/**creamos el modelo a partir de uno ya definido con el esquema anterio */

/**estamos exportando ya el modelo */
module.exports = Mongoose.model("Noticias", NoticiaSchema);
