/**trabajaremos mucho con npm run devstart (npmbre del script) */
/** cada que tenemos un programay hacemos cambios se guardaran
 * es mejor en modo produccion 
 */
const Mongoose = require("mongoose");
const Conexion = "mongodb+srv://AlexisLopez99:OjosMarron18@cluster0.xqhmo.mongodb.net/PruebaConeccion?retryWrites=true&w=majority";

Mongoose.connect(Conexion, {useNewUrlParser:true, useUnifiedTopology:true});

/**Definicion del modelo */
const Usuario = Mongoose.model("Usuario",{ Nombre: String});


/**Creando instancia de ese modelo */
const Usuario1 = new Usuario({Nombre: "Nardine Alsa"});

/**Guardamos ese Usuario, recordando que tambien es una promesa */
//Usuario1.save().then(()=> 
//{
  //  /**Mensaje de comprobacion */
   // console.log("Usuario creado sin problemas");
    /**Buscamos del usuario principal o del modelo todos los datos */
   // Usuario.find().then(console.log);
//})

/**utilizando la funcion async await */
(async () => 
{
    try 
    {
        await Usuario1.save();
        console.log("Usuario creado sin problemas");
        await Usuario.find().then(console.log);
        console.log("Se mostro la informacion");    
    } catch (error) {
        console.log(error);
    }
  
})();
