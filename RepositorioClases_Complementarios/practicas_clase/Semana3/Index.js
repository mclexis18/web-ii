/**estas son nuestras librerias externas */
const Mongoose = require("mongoose");
const Axios = require("axios").default;
const Cheerio = require("cheerio");
const Cron = require("node-cron");

/**Llamamos los archivos de otras partes */
const {MONGO_URI} = require('./config');
const {Noticias} = require ('./models');
const Conexion = "mongodb+srv://AlexisLopez99:OjosMarron18@cluster0.xqhmo.mongodb.net/PruebaConeccion?retryWrites=true&w=majority";

Mongoose.connect(Conexion, {useNewUrlParser:true, useUnifiedTopology:true});

/**una vez por minuto se va a ejecutar */
Cron.schedule('* * * * *', 
async ()=>
{
    //Traemos todos los datos 
    const Html = await Axios.get("https://cnnespanol.cnn.com/");
    // sin el punto data muestra de todos, con el punto data muestra todo los datos del html 
    const $ = Cheerio.load(Html.data);
    const Titulos = $(".news__title");

    let ArregloNoticias = []
    Titulos.each((index, elemento)=> 
    {
       
        const Noticia1 = 
        {
            Titulo: $(elemento).text(),
            Enlace: $(elemento).children().attr("href")
        }
        ArregloNoticias =[...ArregloNoticias, Noticia1];
        
    });
    Noticias.create(ArregloNoticias);
});
