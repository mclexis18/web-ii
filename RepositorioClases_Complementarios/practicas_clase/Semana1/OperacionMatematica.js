/**14 /05/2021 */
function Operacion(N1, N2, Operacion1="Sumar") 
{

    switch (Operacion1) {
        case "Sumar":
            return N1+N2;
            break;
        case "Restar":
            return N1-N2; 
            break;
        case "Multiplicar":
            return N1*N2;
            break;
        case "Dividir":
            return N1/N2;
        default:
            return 0;
            break;
    }

}

function  Saludame(Nombre) 
{
    return `Hola, como estas ${Nombre}`;    
}

/**Exportar una sola funcion */
//module.exports =(Operacion);
/**Desestructurar la funcion */
module.exports = {
    funcion1: Operacion,
    funcion2: Saludame,
    Variable:5
}


