/**Mapas */

const NuevosDatos = new Map();

NuevosDatos.set("Nombre", "Alexis");
NuevosDatos.set("Apellido", "Lopez");
/**Llamamos por medio de la llave */
console.log(NuevosDatos.get("Apellido"));



/**Recorrerlo recordando que su concepto es asociado al de llave valor */
for(let [Llave, Valor] of NuevosDatos)
{
    console.log(`LLaves: ${Llave} Valor: ${Valor}`)
}

/**Tambien podemos elimiinar y los metodos que tiene un arreglo */
NuevosDatos.delete("Apellido");
console.log(NuevosDatos);
