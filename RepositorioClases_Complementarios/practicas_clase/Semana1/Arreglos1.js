const IniciaOtroConcepto=()=>
{
    console.log("Inicia otro concepto");
} 
const Frutas= 
[
    "Pera",
    "Manzana",
    "Guineo",
]
/**Las posiciones se iran ubicando de acuerdo a su posicion ejempli
 * esta es la posicion 0 
 */
const [PrimerFruta] = Frutas;
console.log(`La primer fruta del arreglo es ${PrimerFruta}`);
IniciaOtroConcepto();
/**desestructuramos todo el primer arreglo */
const [Fruta1, Fruta2, Fruta3] = Frutas;
console.log(`Fruta1: ${Fruta1}, Fruta2: ${Fruta2}, Fruta3: ${Fruta3}`);

/**Podemos pasarlo con la funcion express -... */
console.log(...Frutas);

/**Podemos agregar otro valor por el express */

console.log([...Frutas, "Papaya"]);
IniciaOtroConcepto();
const OtrasFrutas =
[
    "Melon",
    "Sandia",
    "Frutilla",
    function(){
        return "Gracias"; 
    }
]
console.log(Frutas);
console.log(OtrasFrutas[3]());

IniciaOtroConcepto();
/**Podemos combinar los dos arreglos que ya tenemos*/
console.log([...Frutas, ...OtrasFrutas]);

IniciaOtroConcepto();
/**El uso del express se usa cuando nos manan a llamar parametros tambien ejemplo */

const EjemploExpress=(...Datos)=>
{
    Datos.forEach((Valores)=> 
    {
      console.log(`Valores por ForEach ${Valores.Valor1}`)
    })
}

const ObjetoNuevo =
{
    Valor1:"X",
    Valor2:"y",
}
EjemploExpress(ObjetoNuevo);

/**Arreglo a objeto */
IniciaOtroConcepto();

/**Las llaves son las posiciones de los arreglos */
console.log({...Frutas});

module.exports = 
{
  ConceptoNuevo: IniciaOtroConcepto   
}


/**Podemos buscar las frutas de manera optima */

if (Frutas.includes("Pera"))
{
    console.log("Felicidades esta disponible");    
}

/**Si no tuvieramos un arreglo definido */
if (["Arroz", "Ceviche", "Tallarin"].includes("Pera"))
{
    console.log("Disponible");    
}else {console.log("No disponible");}