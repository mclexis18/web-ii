/*18 De mayo de 2021* */
let Edad=22;
const Persona = 
{
    Nombre:'Alexis',
    Apellido:'López',
    Edad,
    EsEstudiante:true,
    GetNombreCompleto()
    {
        return `${this.Nombre} ${this.Apellido} `;
    }
}
console.log(Persona.Nombre, Persona.Apellido, Persona.GetNombreCompleto());

const MuestraPersona=(Persona)=> 
{
    console.log(Persona.Nombre);
}
MuestraPersona(Persona);

/**Desestructuracion  */
const MostrarPersonaDe=({Nombre, Apellido})=> 
{
    console.log(`Nombre: ${Nombre}, Apellido: ${Apellido}`);
}
MostrarPersonaDe(Persona);