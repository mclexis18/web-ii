/**20/05/2021 */
/**Inmutabilidad */
const Persona2 = 
{
    Nombre:"Alberto",
    Apellido:"Cedeño",
    Edad:21,
    GeoLocalizacion:
    {
        lat:123.234,
        lng:263.373
    },
    GetNombreCompleto()
    {
        return `Nombre ${this.Nombre}, ${this.Apellido}`
    }
}
/**Las variables desestructuradas deben tener el mismo nombre que las del objeto, caso contrario no funcionara */
const MostrandoObjetos =({Nombre, Apellido, GeoLocalizacion:{lat, lng}})=> 
{
    console.log(`Nombre: ${Nombre}, Apellido: ${Apellido}, Latitud ${lat}`);
}

MostrandoObjetos(Persona2);
const Clonacion = {...Persona2};
/**Los valores cambian de forma indistinta  */
Clonacion.Nombre ="Emmely"
console.log(Clonacion);
console.log(Persona2);
/**Podemos trabajar por referencia o por valor  */
/**
 * 
 * const PersonaNueva = 
{
    Nombre: Persona2.Nombre,
    Apellido: Persona2.Apellido,
    Edad: Persona2.Edad
};
PersonaNueva.Nombre ="Ricardo";

console.log(Persona2);
console.log(PersonaNueva);

 */
