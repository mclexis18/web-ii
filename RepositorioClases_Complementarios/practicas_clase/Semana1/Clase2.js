/**18 de mayo de 2021 */
/**Una clase con un constructor */
class Persona 
{
    constructor(Nombre)
    {
        this.Nombre = Nombre;
    }
    getNombre()
    {
        return this.Nombre;
    }
}

/**Creamos instancia */

const persona = new Persona("Alexis  López");
nombre = persona.getNombre();
console.log(nombre);