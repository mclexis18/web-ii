/**Arrays asociados */

const Prueba =[123,456];

/**Cambiamos los valores de la posicion 0 */
Prueba[0]= 6537;

/**Agregamos nuevos elementos con llave valor */
Prueba["Primero"]="Genesis";
Prueba["Segundo"]="Karen";

/**Vemos el resultado , esta vez no los convierte en un elemento clave valor
 * debido a que pasamos por la funcion express
*/
console.log({...Prueba});