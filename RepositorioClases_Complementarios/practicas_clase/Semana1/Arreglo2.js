//const {ConceptoNuevo} = require('./Arreglos1');
/**Set viene con las estructuras definidas y son restrictivas */

const Numeros = new Set();

Numeros.add(5);
Numeros.add("5");
Numeros.add(5);
Numeros.add(8);
Numeros.add(9);


console.log(Numeros)

/**Podemos ver si se encuentra o no, devuelve un valor de verdadero o falso */

console.log(Numeros.has(5));

/**Podemos eliminarlo */
Numeros.delete(8);
console.log(Numeros);
/**Recorrerlo  */
for(let Valor of Numeros)
{
    console.log(Valor);
}
/**Podemos convertirlo en un array */
const Nuevo = Array.from(Numeros);
console.log(Nuevo);

console.log({Numeros});
