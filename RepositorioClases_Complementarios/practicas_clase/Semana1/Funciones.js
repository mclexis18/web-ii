/*Funciones Normales* */
function  Saludame(Nombre) 
{
    return `Saludos ${Nombre}`    
}
console.log(Saludame("Alex"));
/** funcion anonimas */
const Saludame1 = function (Nombre)
{
    return `Saludame denuevo ${Nombre}`;
} 
console.log(Saludame1("Lopez"));
/**arrow functions */
const Saludame2 =(Nombre)=>
{
    return `Saludame una vez mas ${Nombre}`
}
console.log(Saludame2("Alexis"));

/**Llamar la funcion dentro de otra */
function LlamoSaludo(LLamoFuncion, parametro)
{
    return(LLamoFuncion(parametro));
}
console.log(LlamoSaludo(Saludame2, "Gema"))  ;
