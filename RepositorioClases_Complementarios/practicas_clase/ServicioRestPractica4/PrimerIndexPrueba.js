/**Recursos que no van a cambiar con el tiempo recursos estaticos */
/**este es el primer ejercicio, solo que los quize separar y no comentar para poderlos
 * visualizar mejor 
 */

/**serviciohttp para levantar el servidor */
const http = require('http');

/**Utilizamos la libreria Five system */
const Fs = require('fs');

/**Cuando hay asincronia y sincronica hay funciones diferentes */
/**Asincronia de los multiples Hilos */
const Home = Fs.readFileSync("./Index.html");
const About = Fs.readFileSync("./About.html");
/**Minuto30 */


/**lo que el servidor envia y responde resp and respond */
http.createServer((request, response)=>
{
    /**conozco la peticion de esa manera */
    /**con el request .url puede obtener la peticion de donde se trabaja  */
    /**pedimos la peticion al http://localhost:8080/usuarios */
    
    /**estructura de antres para manejar un servidor  */
    const {url} = request;
    if (url === "/") 
    {
        console.log("Estamos en el home");
        response.writeHead(200, {"Content-type": "text/html"});
        response.write(Home);
    }
    else if (url === "/About") 
    {
        console.log("Estamos en Acerca de ");
        response.writeHead(200, {"Content-type": "text/html"});
        response.write(About);
    } 
    else
    {
        /**el primer parametro son los tipos de errores */
        console.log('Ruta no encontrada');
        response.writeHead(404, {"Content-type": "text/html"});
        response.write("Pagina no Encontrada");
        /**Minuto 45 */
    }
    /**Damos finalizacion a la peticion realizada
     * tambien podemos utilizarlo en cada uno de los apartados
     */
    /**si llega a existir codigo despues del end, no se podra ejecutar */
   /**PUEDE PONERSE EN UN RETURN PARA PODER EJECUTARLO BIEN  */
    response.end();

 /**escuchamos por el puerto 8080 */   
//Minuto 49:10

 /**la funcion es para saber si se esta ejecutando  */
 /**Debes de constatar que los puertos no esten siendo usados */
}).listen(8080, ()=>
{
        console.log("El servidor esta siendo ejecutado puerto 8080");
});
