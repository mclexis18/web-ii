const Express = require('express');
const app = Express();

const PORT = 8080;

//persistencia de datos
let Arreglo = [];

/**cualquier nombre exponemos las carpetas y recursos estaticos */
/**se ingresa desde la ruta public y de esa rruta podemos acceder a todo neustro
 * contenido HTML
 */
app.use('/public', Express.static( __dirname +'/public'));

/**siempre necesitaremos el express con los recursos dinamicos */
/**este es un middelware */
app.use(Express.json());

//esta viene siendo el API
/// http://localhost:3000/api/v1/Estudiantes

//get

//devuelva todos los estudiantes
/**resol viene siendo nuestra persistencia y nuestro arreglo en este caso */
/**Primer Ruta */
app.get('/', (req, res)=> 
{
    /**esponemos el arreglo con el res, res lo que respondemos, reques lo que el usuario envia */
    /**dentro de ese send podemos devolver texto plano o json */
    res.send(Arreglo);
});

//get

// estudiantes por id

/**el enlace depende de lo que queramos acceder la ruta */
app.get('/:Nombre',  (req, res)=>
{
    const {Nombre} =req.params;
    const Respuesta = Arreglo.filter(p=> 
        {
            return p.Nombre === Nombre;    
        });
        if (Respuesta.length>0)
        {
            res.status(202).send(
                {
                    respuesta:Respuesta[0]
                }
            )
        }else
        {
            res.status(404).send(
                {
                    respuesta:"No se pudo encontrar"
                })
        }
    console.log(Respuesta);     
});



//post
//debe de  ponerse el archivo json al momento de enviar
/**quiero decir para que envie json */
app.post('/', (req, res)=>
{
    /**le enviamos al body lo que queremos */
    const {body} = req;
    Arreglo.push(body) 
    res.status(201).send
    (
        {
            Mensaje:"se ejecuta correctamente",
            Respuesta:body

        }
    )
});
/**1 hpora */

//body{nombre:'Alex' curso:'sexto b'}

//misma ruta que se maneja en los get 

/**embairomen son entornos de trabajo */
/**Minuto 46:47 alminuto 54:20 se habla de postman */

//put y patch
app.put('/', (req, res)=>
{
    const {Nombre, Curso} = req.body;
    let Elemento = Arreglo.filter(p=> p.Nombre === Nombre)[0];
    Elemento.Curso = Curso;
    res.status(200).send(
        {
            Respuesta:Elemento
        }
        );

});

//misma ruta
//enviaremos un body 
//body{nombre:'Alex' curso:'sexto A'}



//delete

app.delete('/', (req, res)=>
{
    const {Nombre} = req.params;
    Arreglo= Arreglo.filter(p=> p.Nombre !== Nombre);
    res.status(200).send({
        respuesta:"se elimino correctamente"
    });

});
//pasar como parametro el id 
//put push and delete se manejan con la rta principal
app.listen(PORT, ()=>
{
    console.log(`el servidor esta corriendo por el puerto ${PORT}`);
});