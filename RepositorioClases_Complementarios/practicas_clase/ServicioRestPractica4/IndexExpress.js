
const Fs = require('fs');

const Express = require('express');


/**listas blancas y listas negras con cors es como un tipo 
 * de filtrado allow and denied
 */

 /**Es una libreria de terceros que debe ser instalada */
 const cors = require('cors');

const Path = require("path");
 
/**Creamos una constante con el puerto */
const PUERTO = 3000;
/**Llamamos al home y al abour con la libreria fs */
const Home = Fs.readFileSync("./public/Index.html");
const About = Fs.readFileSync("./public/About.html");
/**podriamos usar http para proxy? */


/**creamos un servidor de express */
const Server = Express();

/**¿Arquitectura en capas? */

/**midelware agregamos un midelware con .use */
/**como un triger, cada que envia un dato se dispara .-> */

Server.use(cors()).use(Express.json());

Server.get("/", DevolvemosIndex);

Server.get("/About", (req, res)=> 
{
   res.write(About);
   res.end();
});
Server.listen(PUERTO, ()=> {
    console.log(`Servidor ejecutando por el puerto ${PUERTO}`);
});

/**1:22:18 seconds */
/**dirname es como una constante para conocer la carpeta actual
 * dirnme es como la carpeta actual 
 */

 /**con el Path podemos concatenar  como si fuera una sola ruta  */
const PaginaDeError = Path.join( __dirname, './public/Error.html');

/**1:24:33 */
Server.use((req, res, next)=> 
{
    /**devolvemos la pagina de error */
    res.status(404).sendFile(PaginaDeError);

})


/**el next viene siendo como un en caso de puedes continuar */
function DevolvemosIndex(req, res)
{
    res.write(Home);
    res.end();

}

/**el midelware se inyecta en la peticion son como trigers en base de datos */
/**midelware para validar informacion como or ejemplo las validaciones
 * de los formularios
 * ¿samitizar? ---cosas sospechosas de la base de datos 
 */