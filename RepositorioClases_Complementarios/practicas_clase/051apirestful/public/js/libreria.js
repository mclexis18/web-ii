window.addEventListener('load', function () {

    let HtmlGenerado="";
    HtmlGenerado+= `
    <label for="txtid">ID</label>
    <input type="text" id="txtid">
    <label for="txtname">Nombre</label>
    <input type="text" id="txtname">
    <label for="txtusername">Nombre del Usuario</label>
    <input type="text" id="txtusername">
    <label for="txtpassword">Contraseña</label>
    <input type="text" id="txtpassword">
    <button id="btnnuevo"> Nuevo</button>
    <button id="btngrabar">Grabar</button>
    <button id="btnmodificar">Modificar</button>
    <button id="btnconsultar">Consultar</button>
    <button id="btneliminar"> Eliminar</button>
    <div id="divcontenido"></div>`;

    htmlCuerpo.innerHTML = HtmlGenerado;


    btnnuevo.addEventListener('click', function (){

        
        txtid.value='';
        txtname.value='';
        txtusername.value='';
        txtpassword.value = '';

    });

    btngrabar.addEventListener('click', function () {
        
        let URL = `http://localhost:5000/api/v1/user`;
        let data = {

            name: txtname.value,
            username: txtusername.value,
            password: txtpassword.value
        }

        fetch(URL, {

            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type':'application/json'
            }

        }).then(res=> res.json())
        .then(res2=> console.log(res2))
        .catch(error=> console.error('error', error))
        
    })

    btnmodificar.addEventListener('click', function(){
        
        let URL = `http://localhost:5000/api/v1/user/${txtid.value}`;
        let data = {

            name: txtname.value,
            username: txtusername.value,
            password: txtpassword.value
        }

        fetch(URL, {
            method: 'PATCH',
            body: JSON.stringify(data),
            headers:{
                'Content-Type':'application/json'
            }

        }).then(res=> res.json())   
        .catch(error=> console.error('error', error));

    })

    btnconsultar.addEventListener('click', function () {
        fetch(`http://localhost:5000/api/v1/user`).then(resultado=>{
        return resultado.json()
    }).then(consulta=>{
        console.log(consulta);
        let tabla= "<table border=1>"
        for(const indiceElemento in consulta)
        {
            tabla+="<tr>";
            const actual =  consulta[indiceElemento];
            tabla+=`<td>${actual.name}</td>`
            tabla+=`<td>${actual.password}</td>`
            tabla+=`<td> <button value='${actual._id}'>${actual.username}</button> </td>`
            tabla+="</tr>"
        }
        tabla+="</table>"
        divcontenido.innerHTML= tabla;

    })

    })

    btneliminar.addEventListener('click', function () {

        let URL = `http://localhost:5000/api/v1/user/${txtid.value}`;

        fetch(URL, {

            method:'DELETE'

        }).then(res=> res.json())
        .then(res2=> console.log(res2))
        .catch(error=> console.error('error', error));

    })
    
})
