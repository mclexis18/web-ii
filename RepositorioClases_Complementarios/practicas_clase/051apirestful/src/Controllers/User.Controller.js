const { AwilixError } = require("awilix");

let _UserServices = null;
class UserController {

    constructor({UserServices}){

        _UserServices = UserServices

    }

    //get
    async get(req, res){

        const {userId} = req.params;
        const User = await _UserServices.get(userId);
        return res.send(User);

    } 
    //getAll

    async getAll(req, res){
        const users = await _UserServices.getAll();
        return res.send(users);
    }

    //create
    async create(req, res){

        const {body} =req; 
        const CreateUser = await _UserServices.create(body);
        return res.send(CreateUser);

    }

    //update
    async update(req, res){

        const {userId} = req.params;
        const {body} = req;
        const UpdateUser = await _UserServices.update(userId, body);
        return res.send(UpdateUser);

    }

    //delete
    async delete(req, res){

        const {userId} =req.params;
        const DeleteUser = await _UserServices.delete(userId);
        return res.send(DeleteUser);

    }

}

module.exports = UserController;