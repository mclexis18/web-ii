/**La rutas son dependiendo de los niveles, por ejemplo nivel1, hasta el nivel 3  */

/**Los niveles son dependiendo de las rutas que tengamos por ejemplo  */

/**Nivel1:Localhost:8080 */

/**Nivel2:Localhost:8080/About */

/**Nivel3: Localhost:8080/About/Api */

const Express = require('express');
const Cors = require('cors');
const Helmet = require('helmet');
const Compression = require('compression');
const Express_async = require('express-async-errors');



/**Los routes que ya habiamos definido en el Index.Routes */
module.exports = function({HomeRoutes, UserRoutes})
{
    const Route1 = Express.Router();
    const ApiRoutes = Express.Router();

    /**todos estos recursos seran aplicados en Nuestro servicio */
    ApiRoutes
    .use(Express.json())
    .use(Cors())
    .use(Helmet())
    .use(Compression());

    /**Sobre esta ruta se pueden aplicar Varios recursos por ejemplo podemos tener mas rutas
     * y el router padre se encargará de ellas
    */
    ApiRoutes.use('/Home', HomeRoutes);
    
    ApiRoutes.use('/User', UserRoutes);

    /**Aqui van a ir todas las rutas que hemos definidos, o que definiremos en nuetsro proyecto
     * este es el router padre
     */
    Route1.use('/api/v1', ApiRoutes); 
    /**podriamos usar dos versiones en una misma Api*/

    Route1.use('/Public', Express.static('public'));
    

    /**Devolvemos el Router padre */
    return Route1;

    /**http:localhost:8080/v1/api/Home dentro del HomeRoutes llegamos al index */

}