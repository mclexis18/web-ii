const {Router} =require('express');

module.exports= function({UserController}){

    const router = Router();
    /**Si no hay los dos puntos eso se vuelve estatico  */
    router.get('/:userId', UserController.get);
    router.get('', UserController.getAll);
    router.post('', UserController.create);
    router.patch('/:userId', UserController.update);
    router.delete('/:userId', UserController.delete);
    /**1:07:51 */
    return router;

}