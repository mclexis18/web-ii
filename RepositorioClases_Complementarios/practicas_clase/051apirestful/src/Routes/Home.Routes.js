const {Router} = require('express');


module.exports = function({HomeController}){
    
    const router = Router();

    /**Llamamos al Homecontroller que ya tiene el metodo del services
     * en otras palabras que ya tiene el servicio 
     */
    router.get('/', HomeController.Index);
    router.get('/Saludos', HomeController.Saludos);
    
    return router;
}

/**Cada que se crea algo va al contenedor */