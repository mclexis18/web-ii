const BaseService = require('./Base.Services');
/**Se extiende de la clase padre */
let _UserRepository = null

class UserService extends BaseService{

    constructor({UserRepository}){

        super(UserRepository);
        /**Tenemos accedo a la inyección */
        _UserRepository = UserRepository

    }
    async getUserByUserName(username){

        return await this.UserRepository.getUserByUserName(username);

    }
    /**Se pueden agregar mas metodos desde aqui, siempre y cuando allamos definido los repositorios correctamente
     */

    

}
module.exports = UserService;