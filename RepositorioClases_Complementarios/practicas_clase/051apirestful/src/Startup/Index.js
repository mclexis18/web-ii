/**Capa de Inicializacion */
const Express = require('express');


let _Express = null;
let _Config = null;

class Server 
{
    /**Inyeccion de dependencias */
    constructor({Routes, Config})
    {
        _Config = Config;
        _Express = Express().use(Routes);
    }
    /**Iniciamos la app con un metodo */

    Start ()
    {
        return new Promise(resolve=>
        {
            _Express.listen(_Config.PORT || 5000, ()=> 
             {
                console.log(`El servidor ${_Config.APPLICATION_NAME} esta corriendo por el puerto ${_Config.PORT}`);
                /**resolvemos */
                resolve();
             })
        });
        
    }
}
/**Ser cuidadoso con la exportacion, eso puede afectar a los dockers */

module.exports = Server; 