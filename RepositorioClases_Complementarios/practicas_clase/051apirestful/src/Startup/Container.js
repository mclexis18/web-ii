/**Todo lo que queremos que se comunique */
/**Aquello que se va a desestricturar */
const {createContainer, asClass, asValue, asFunction} = require('awilix');

/**Agregar variables de entorno */
const Config = require('../config/Index');

/**La appi de esta misma carpeta  */
const App = require('./Index');

/**los services como clases, las rutas como funciones, modelo como valores
 * Variables de entorno como valores tambien 
 */
/**Home */


const {HomeService,UserServices} = require('../Services/Index');

const {HomeController, UserController}= require('../Controllers/Index');

const {HomeRoutes, UserRoutes} =  require('../Routes/Index.Routes');

const Routes =require('../Routes/Index');


/**Modelos */
const {User} = require('../Models/Index');

/**Reposirtorios */
const {UserRepository} =require('../Repositories/Index');







 /**Toda la parte de la arquitectura en el contenedor  */
 /**contenedores en el backend para compartir informacion o dependencias */
 const Contenedor = createContainer();

 /**Registra los modulos para compartir información con el 
  * .register
  */

 /**Tambien lo registramos como si fuera un arreglo  */
 Contenedor
 .register(
      {
           App: asClass(App).singleton(),
           Routes: asFunction(Routes).singleton(),
           Config: asValue(Config)

      }
 )
 .register(
     {
         /**La estamos trabajando como clases el singleton solo instancia la clase una soila vez*/
          /**singleton para que no se pueda crear otra instancia  */
           HomeService:asClass(HomeService).singleton(),
           UserServices:asClass(UserServices).singleton()
         /**la diferencia entre controller y services, es que los controlladores son handdelrs que son llamados
          * desde la ruta 
          */
     })
     .register(
        {
             HomeController:asClass(HomeController.bind(HomeController)).singleton(),
             UserController:asClass(UserController.bind(UserController)).singleton(),

        })
     .register(
         {
              HomeRoutes: asFunction(HomeRoutes).singleton(),
              UserRoutes:asFunction(UserRoutes).singleton()
         })
       .register(
            {
                 User:asValue(User)
            })
        .register(
             {
                  UserRepository:asClass(UserRepository).singleton()
             })    
/**en el handdelrs se denomina el scope, es algo asi
 * como el contexto dentro de las funciones flechas
 * Para evitar que  pierda el contexto se puede poner con .bind
 */




 module.exports= Contenedor; 